package jp.aegif.nemaki.api.resources;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import jp.aegif.nemaki.model.User;
import jp.aegif.nemaki.util.PasswordHasher;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
@Path("/user")
public class UserResource extends ResourceBase {

	@SuppressWarnings("unchecked")
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public String list() {
		boolean status = true;
		JSONObject result = new JSONObject();
		JSONArray listJSON = new JSONArray();
		JSONArray errMsg = new JSONArray();

		// Get the all users list
		List<User> userList;
		try {
			userList = userGroupService.getUsers();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			
			for(User user : userList){
				String id = user.getId();
				String name = user.getName();
				String firstName = user.getFirstName();
				String lastName = user.getLastName();
				String email = user.getEmail();
				String parentId = user.getParentId();
				String path = user.getPath();
				String type = user.getType();
				String creator = user.getCreator();
				String created = new String();
				try{
					if(user.getCreated() != null){
						created = sdf.format(user.getCreated().getTime());
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				String modifier = user.getModifier();
				String modified = new String();
				try{
					if(user.getModified() != null){
						modified = sdf.format(user.getModified().getTime());
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
				JSONObject userJSON = new JSONObject();
				userJSON.clear();
				userJSON.put(ITEM_USERID, user.getId());
				userJSON.put(ITEM_USERNAME, user.getName());
				userJSON.put(ITEM_FIRSTNAME, user.getFirstName());
				userJSON.put(ITEM_LASTNAME, user.getLastName());
				userJSON.put(ITEM_EMAIL, user.getEmail());
				userJSON.put(ITEM_PARENTID, user.getParentId());
				userJSON.put(ITEM_PATH, user.getPath());
				userJSON.put(ITEM_TYPE, user.getType());
				userJSON.put(ITEM_CREATOR, user.getCreator());
				userJSON.put(ITEM_CREATED, created);
				userJSON.put(ITEM_MODIFIER, user.getModifier());
				userJSON.put(ITEM_MODIFIED, modified);
				listJSON.add(userJSON);
			}
			result.put("users", listJSON);
		} catch (Exception e) {
			e.printStackTrace();
			addErrMsg(errMsg, ITEM_ALLUSERS, ERR_LIST);
		}
		result = makeResult(status, result, errMsg);
		return result.toJSONString();
	}

	@POST
	@Path("/create/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String create(@PathParam("id") String id,
			@FormParam(FORM_USERNAME) String name,
			@FormParam(FORM_PASSWORD) String password,
			@FormParam(FORM_FIRSTNAME) String firstName,
			@FormParam(FORM_LASTNAME) String lastName,
			@FormParam(FORM_EMAIL) String email,
			@FormParam(FORM_CREATOR) String creator) {

		boolean status = true;
		JSONObject result = new JSONObject();
		JSONArray errMsg = new JSONArray();

		// Validation
		status = newUserValidation(errMsg, id, name, password);

		// Create a user
		if (status) {
			// initialize mandatory but space-allowed parameters
			if (firstName == null)
				firstName = "";
			if (lastName == null)
				lastName = "";
			if (email == null)
				email = "";
			if (creator == null)
				creator = "";

			// Generate a password hash
			String passwordHash = PasswordHasher.hash(password);

			User user = new User(id, name, firstName, lastName, email,
					PARENTID, PATH, TYPE_USER, passwordHash, creator,
					millisToCalendar(System.currentTimeMillis()));

			try {
				userGroupService.createUser(user);
			} catch (Exception ex) {
				ex.printStackTrace();
				status = false;
				addErrMsg(errMsg, ITEM_USER, ERR_CREATE);
			}
		}
		result = makeResult(status, result, errMsg);
		return result.toJSONString();
	}

	@PUT
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String update(@PathParam("id") String id,
						 @FormParam(FORM_PASSWORD) String password, 
						 @FormParam(FORM_USERNAME) String name, 
			 			 @FormParam(FORM_FIRSTNAME) String firstName,
			 			 @FormParam(FORM_LASTNAME) String lastName,
			 			 @FormParam(FORM_EMAIL) String email,
			 			 @FormParam(FORM_MODIFIER) String modifier,
			 			 @FormParam("admin") String admin,
			 			 @FormParam("adminpass") String adminpass){
		boolean status = true;
		boolean cmpPass = true;
		JSONObject result = new JSONObject();
		JSONArray errMsg = new JSONArray();
		User user = new User();

		// Validation
		if (!nonZeroString(name)) {
			status = false;
			addErrMsg(errMsg, ITEM_USERNAME, ERR_MANDATORY);
		}
		try {
			user = userGroupService.getUserById(id);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = false;
			addErrMsg(errMsg, ITEM_USER, ERR_NOTFOUND);
		}
		
		if(status){
			cmpPass = PasswordHasher.isCompared(password, user.getPasswordHash());
			if(isAdmin(admin, adminpass)) cmpPass = true;		//Admin permission check
			if(!cmpPass){
				status = false;
				addErrMsg(errMsg, ITEM_PASSWORD, ERR_WRONGPASSWORD);
			}
		}
		
		//Edit & Update
		if(status){
			//Edit the user info
			//if a parameter is not input, it won't be modified.
			if(name != null) user.setName(name);			
			if(firstName != null) user.setFirstName(firstName);
			if(lastName != null) user.setLastName(lastName);
			if(email != null) user.setEmail(email);
			if(modifier == null) modifier = "";
			user.setModifier(modifier);
			user.setModified(millisToCalendar(System.currentTimeMillis()));

			try {
				userGroupService.updateUser(user);
			} catch (Exception ex) {
				ex.printStackTrace();
				status = false;
				addErrMsg(errMsg, ITEM_USER, ERR_NOTFOUND);
			}
		}
		result = makeResult(status, result, errMsg);
		return result.toJSONString();
	}

	@PUT
	@Path("/updatepassword/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updatePassword(@PathParam("id") String id,
			 					 @FormParam(FORM_PASSWORD) String password,
			 					 @FormParam(FORM_NEWPASSWORD) String newPassword,
			 					 @FormParam(FORM_MODIFIER) String modifier,
			 					 @FormParam("admin") String admin,
					 			 @FormParam("adminpass") String adminpass){
		boolean status = true;
		boolean cmpPass = true;
		JSONObject result = new JSONObject();
		JSONArray errMsg = new JSONArray();
		User user = new User();

		// Validation
		if (!nonZeroString(newPassword)) {
			status = false;
			addErrMsg(errMsg, ITEM_NEWPASSWORD, ERR_MANDATORY);
		}

		try {
			user = userGroupService.getUserById(id);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = false;
			addErrMsg(errMsg, ITEM_USER, ERR_NOTFOUND);
		}
		
		if(status){
			cmpPass = PasswordHasher.isCompared(password, user.getPasswordHash());
			if(isAdmin(admin, adminpass)) cmpPass = true;		//Admin permission check
			if(!cmpPass){
				status = false;
				addErrMsg(errMsg, ITEM_PASSWORD, ERR_WRONGPASSWORD);
			}
		}

		// Edit & Update
		if (status) {
			// edit password
			String passwordHash = PasswordHasher.hash(newPassword);
			user.setPasswordHash(passwordHash);
			if (modifier == null)
				modifier = "";
			user.setModifier(modifier);
			user.setModified(millisToCalendar(System.currentTimeMillis()));

			// Write to DB
			try {
				userGroupService.updateUser(user);
			} catch (Exception ex) {
				ex.printStackTrace();
				status = false;
				addErrMsg(errMsg, ITEM_USER, ERR_UPDATEPASSWORD);
			}
		}
		result = makeResult(status, result, errMsg);
		return result.toJSONString();
	}

	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String delete(@PathParam("id") String id,
			 			 @FormParam(FORM_PASSWORD) String password,
			 			 @FormParam("admin") String admin,
			 			 @FormParam("adminpass") String adminpass){
		boolean status = true;
		boolean cmpPass = true;
		JSONObject result = new JSONObject();
		JSONArray errMsg = new JSONArray();
		User user = new User();

		// Validation
		try {
			user = userGroupService.getUserById(id);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = false;
			addErrMsg(errMsg, ITEM_USER, ERR_NOTFOUND);
		}
		
		if(status){
			cmpPass = PasswordHasher.isCompared(password, user.getPasswordHash());
			if(isAdmin(admin, adminpass)) cmpPass = true;		//Admin permission check
			if(!cmpPass){
				status = false;
				addErrMsg(errMsg, ITEM_PASSWORD, ERR_WRONGPASSWORD);
			}
		}

		// Delete a user
		if (status) {
			try {
				userGroupService.deleteUser(id);
			} catch (Exception ex) {
				ex.printStackTrace();
				status = false;
				addErrMsg(errMsg, ITEM_USER, ERR_DELETE);
			}
		}
		result = makeResult(status, result, errMsg);
		return result.toJSONString();
	}

	private boolean newUserValidation(JSONArray errMsg, String userId,
			String userName, String password) {
		boolean status = true;
		if (!nonZeroString(userId)) {
			status = false;
			addErrMsg(errMsg, ITEM_USERID, ERR_MANDATORY);
		}

		if (!nonZeroString(userName)) {
			status = false;
			addErrMsg(errMsg, ITEM_USERNAME, ERR_MANDATORY);
		}

		if (!nonZeroString(password)) {
			status = false;
			addErrMsg(errMsg, ITEM_PASSWORD, ERR_MANDATORY);
		}
		return status;
	}

}
