/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.api.resources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.ConfigurationException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//import org.codehaus.jettison.json.JSONObject;
import org.json.simple.JSONObject;
import org.ho.yaml.Yaml;

/**
 * TODO This class is under construction.
 * 
 * @author mryoshio
 */
@Path("/aspects")
public class AspectResource {

	private static Map<String, Object> baseModel = null;
	private static final String baseModelFile = "base_model.yml";

	public AspectResource() {
		try {
			loadYml();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(AspectResource.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (ConfigurationException ex) {
			Logger.getLogger(AspectResource.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	@GET
	@Path("/hello")
	@Produces(MediaType.TEXT_PLAIN)
	public String test() {
		return "Hello Jersey!";
	}

	/*@GET
	@Path("/base")
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject base() {
		return new JSONObject(baseModel);
	}*/
	
	@GET
	@Path("/base")
	@Produces(MediaType.APPLICATION_JSON)
	public String base() {
		return (new JSONObject(baseModel)).toString();
	}

	private synchronized void loadYml() throws FileNotFoundException,
			ConfigurationException {
		InputStream input = getClass().getClassLoader().getResourceAsStream(
				baseModelFile);
		if (null == input) {
			throw new ConfigurationException(baseModelFile + " not found.");
		}
		Object o = Yaml.load(input);
		baseModel = (Map<String, Object>) ((Map) o).get("aspects");
		//debug
		System.out.println("[aegif]o:" + o.toString());
		System.out.println("[aegif]baseModel:" + baseModel.toString());
		try {
			if (input != null) {
				input.close();
			}
		} catch (IOException ex) {
			Logger.getLogger(AspectResource.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}
}
