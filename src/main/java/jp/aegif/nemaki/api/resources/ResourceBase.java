package jp.aegif.nemaki.api.resources;

import java.util.GregorianCalendar;
import java.util.TimeZone;

import jp.aegif.nemaki.model.User;
import jp.aegif.nemaki.service.UserGroupService;
import jp.aegif.nemaki.service.impl.CouchDaoServiceImpl;
import jp.aegif.nemaki.util.PasswordHasher;
import jp.aegif.nemaki.util.PropertyManager;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ResourceBase {
	
	protected ApplicationContext context;
	protected UserGroupService userGroupService;
	protected CouchDaoServiceImpl daoService;

	static final String DATE_FORMAT = "yyyy:MM:dd HH:mm:ss z";
	static final String PATH = "dummy";
	static final String TYPE_USER = "aegif:user";
	static final String TYPE_GROUP = "group";
	static final String PARENTID = "/";
	static final String SUCCESS = "success";
	static final String FAILURE = "failure";
	static final String DOCNAME_VIEW="_design/_repo";
	static final String FILEPATH_PROPERTIESFILE = "nemakiware.properties";
	static final String PROPERTY_REPOSITORIES = "nemakiware.repositories";
	static final String PROPERTY_INFO_REPOSITORY = "nemakiware.info.repository";
	static final String PROPERTY_DBHOST = "db.host";
	static final String PROPERTY_DBPORT = "db.port";
	static final String PROPERTY_DBPROTOCOL = "db.protocol";
	static final String VIEW_ALL = "_all_dbs";
	static final String SPACE = "";
	
	static final String FILEPATH_VIEW = "views.json";
	
	static final String API_ADD = "add";
	static final String API_REMOVE = "remove";
	
	static final String FORM_USERNAME = "name";
	static final String FORM_PASSWORD = "password";
	static final String FORM_NEWPASSWORD = "newPassword";
	static final String FORM_FIRSTNAME = "firstName";
	static final String FORM_LASTNAME = "lastName";
	static final String FORM_EMAIL = "email";
	static final String FORM_CREATOR = "creator";
	static final String FORM_MODIFIER = "modifier";
	static final String FORM_GROUPNAME = "name";
	static final String FORM_MEMBER_USERS = "users";
	static final String FORM_MEMBER_GROUPS = "groups";
	static final String FORM_ID = "id";
	
	static final String ITEM_USERID = "userId";
	static final String ITEM_USER = "user";
	static final String ITEM_USERNAME = "userName";
	static final String ITEM_PASSWORD = "password";
	static final String ITEM_NEWPASSWORD = "newPassword";
	static final String ITEM_FIRSTNAME = "firstName";
	static final String ITEM_LASTNAME = "lastName";
	static final String ITEM_EMAIL = "email";
	static final String ITEM_PARENTID = "parentId";
	static final String ITEM_PATH = "path";
	static final String ITEM_TYPE = "type";
	static final String ITEM_CREATOR = "creator";
	static final String ITEM_CREATED = "created";
	static final String ITEM_MODIFIER = "modifier";
	static final String ITEM_MODIFIED = "modified";
	static final String ITEM_GROUPID = "groupId"; 
	static final String ITEM_GROUP = "group";
	static final String ITEM_GROUPNAME = "groupName"; 
	static final String ITEM_MEMBER_USERS = "users";
	static final String ITEM_MEMBER_GROUPS = "groups";
	static final String ITEM_MEMBER_USERSSIZE = "usersSize";
	static final String ITEM_MEMBER_GROUPSSIZE = "groupsSize";
	static final String ITEM_ALLUSERS = "users";
	static final String ITEM_ALLGROUPS = "groups";
	static final String ITEM_STATUS = "status";
	static final String ITEM_ERROR = "error";
	static final String ITEM_COUCHDBRESPONSE = "couchDbResponse";
	static final String ITEM_COUCHDBRESTURL = "couchDBRestTUrl";
	static final String ITEM_DATABASE = "database";
	static final String ITEM_URL = "url";
	static final String ITEM_DATABASES = "databases";
	static final String ITEM_VIEW = "view";
	static final String ITEM_PROPERTIESFILE = "propertiesFile";
	
	
	static final String ERR_MANDATORY = "mandatory";
	static final String ERR_ALREADYMEMBER = "alreadyMember";
	static final String ERR_NOTMEMBER = "notMember";
	static final String ERR_LIST = "failToList";
	static final String ERR_CREATE = "failToCreate";
	static final String ERR_UPDATE = "failToUpdate";
	static final String ERR_UPDATEPASSWORD = "failToUpdatePassword";
	static final String ERR_UPDATEMEMBERS = "failToUpdateMembers";
	static final String ERR_DELETE = "failToDelete";
	static final String ERR_NOTFOUND = "notFound";
	static final String ERR_WRONGPASSWORD = "wrong";
	static final String ERR_PARSEJSON = "failToParseStringToJSON";
	static final String ERR_PARSEURL = "failToParseUrl";
	static final String ERR_GROUPITSELF = "failToAddGroupToItself";
	static final String ERR_READ = "failToRead";
	static final String ERR_STATUSCODE = "statusCode:";
	static final String ERR_ADD_REPOSITORY = "failToAddRepository";
	static final String ERR_REMOVE_REPOSITORY = "failToRemoveRepository";
	
	//Set daoService
	public ResourceBase(){
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
	    userGroupService = (UserGroupService)context.getBean("UserGroupService");
	    daoService = (CouchDaoServiceImpl)context.getBean("daoService");
	}

	//Utility methods
	protected JSONArray addErrMsg(JSONArray errMsg, String item, String msg){
		JSONObject obj = new JSONObject();
		obj.put(item, msg);
		errMsg.add(obj);
		return errMsg;
	}
	
	protected JSONObject makeResult(boolean status, JSONObject result, JSONArray errMsg){
		if(status && errMsg.size() == 0){
			result.put(ITEM_STATUS, SUCCESS);
		}else{
			result.put(ITEM_STATUS, FAILURE);
			result.put(ITEM_ERROR, errMsg);
		}
		return result;
	}
	
	protected boolean nonZeroString(String param){
		if (param == null || param.equals("")){
			return false;
		}else{
			return true;
		}
	}
	protected GregorianCalendar millisToCalendar(long millis) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
		calendar.setTimeInMillis(millis);
		return calendar;
	}
	
	protected boolean isAdmin(String id, String password){
		if(!nonZeroString(id) || !nonZeroString(password)) return false;
		
		String defaultAdminUserName = new String();
		try{
			PropertyManager propertyManager = new PropertyManager(FILEPATH_PROPERTIESFILE);
			defaultAdminUserName = propertyManager.readValue("defaultAdminUserName");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		User user = userGroupService.getUserById(id);
		if(user.getId().equals(defaultAdminUserName)){
			//password check
			boolean cmpPass = PasswordHasher.isCompared(password, user.getPasswordHash());
			if(cmpPass) return true;
		}
		return false;
	}
}
