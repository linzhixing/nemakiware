/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.repository;

import java.math.BigInteger;
import java.util.List;

import jp.aegif.nemaki.service.ACLService;
import jp.aegif.nemaki.service.DiscoveryService;
import jp.aegif.nemaki.service.NavigationService;
import jp.aegif.nemaki.service.NemakiCmisService;
import jp.aegif.nemaki.service.ObjectService;
import jp.aegif.nemaki.service.RepositoryService;

import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.ExtensionsData;
import org.apache.chemistry.opencmis.commons.data.FailedToDeleteData;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderContainer;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderList;
import org.apache.chemistry.opencmis.commons.data.ObjectList;
import org.apache.chemistry.opencmis.commons.data.ObjectParentData;
import org.apache.chemistry.opencmis.commons.data.Properties;
import org.apache.chemistry.opencmis.commons.data.RenditionData;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionContainer;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionList;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.ObjectInfoHandler;
import org.apache.chemistry.opencmis.commons.spi.Holder;

/**
 * Nemaki repository
 */
public class NemakiRepository {

	private ACLService aclService;
	private DiscoveryService discoveryService;
	private NavigationService navigationService;
	private ObjectService objectService;
	private RepositoryService repositoryService;

	// -- Object Service

	public ObjectData create(CallContext callContext, Properties properties,
			String folderId, ContentStream contentStream,
			VersioningState versioningState, ObjectInfoHandler objectInfos) {

		return objectService.create(callContext, properties, folderId,
				contentStream, versioningState, objectInfos);
	}

	public void setContentStream(CallContext callContext,
			Holder<String> objectId, boolean overwriteFlag,
			ContentStream contentStream) {

		objectService.setContentStream(callContext, objectId, overwriteFlag,
				contentStream);
	}

	public FailedToDeleteData deleteTree(CallContext callContext,
			String folderId, Boolean continueOnFailure) {

		return objectService.deleteTree(callContext, folderId,
				continueOnFailure);
	}

	public void deleteObject(CallContext callContext, String objectId) {
		
		objectService.deleteObject(callContext, objectId);
	}

	public void moveObject(CallContext callContext, Holder<String> objectId,
			String targetFolderId, NemakiCmisService couchCmisService) {

		objectService.moveObject(callContext, objectId, targetFolderId,
				couchCmisService);
	}

	public List<RenditionData> getRenditions(CallContext callContext,
			String objectId, String renditionFilter, BigInteger maxItems,
			BigInteger skipCount, ExtensionsData extension) {

		return objectService.getRenditions(callContext, objectId,
				renditionFilter, maxItems, skipCount, extension);
	}

	public void updateProperties(CallContext callContext,
			Holder<String> objectId, Properties properties,
			NemakiCmisService couchCmisService) {

		objectService.updateProperties(callContext, objectId, properties,
				couchCmisService);
	}

	public ContentStream getContentStream(CallContext callContext,
			String objectId, BigInteger offset, BigInteger length) {

		return objectService.getContentStream(callContext, objectId, offset,
				length);
	}

	public ObjectData getObjectByPath(CallContext callContext, String path,
			String filter, Boolean includeAllowableActions, Boolean includeAcl,
			ObjectInfoHandler objectInfos) {

		return objectService.getObjectByPath(callContext, path, filter,
				includeAllowableActions, includeAcl, objectInfos);
	}

	public ObjectData getObject(CallContext callContext, String objectId,
			String versionServicesId, String filter,
			Boolean includeAllowableActions, Boolean includeAcl,
			ObjectInfoHandler objectInfos) {

		return objectService.getObject(callContext, objectId,
				versionServicesId, filter, includeAllowableActions, includeAcl,
				objectInfos);
	}

	public AllowableActions getAllowableActions(CallContext callContext,
			String objectId) {

		return objectService.getAllowableActions(callContext, objectId);
	}

	public String createFolder(CallContext callContext, Properties properties,
			String folderId) {

		return objectService.createFolder(callContext, properties, folderId);
	}

	public String createDocumentFromSource(CallContext callContext,
			String sourceId, Properties properties, String folderId,
			VersioningState versioningState) {

		return objectService.createDocumentFromSource(callContext, sourceId,
				properties, folderId, versioningState);
	}

	public String createDocument(CallContext callContext,
			Properties properties, String folderId,
			ContentStream contentStream, VersioningState versioningState) {

		return objectService.createDocument(callContext, properties, folderId,
				contentStream, versioningState);
	}

	// --- Navigation Service

	public ObjectInFolderList getChildren(CallContext callContext,
			String folderId, String filter, Boolean includeAllowableActions,
			Boolean includePathSegments, BigInteger maxItems,
			BigInteger skipCount, ObjectInfoHandler objectInfos) {

		return navigationService.getChildren(callContext, folderId, filter,
				includeAllowableActions, includePathSegments, maxItems,
				skipCount, objectInfos);
	}

	public List<ObjectInFolderContainer> getDescendants(
			CallContext callContext, String folderId, BigInteger depth,
			String filter, Boolean includeAllowableActions,
			Boolean includePathSegment, ObjectInfoHandler objectInfos,
			boolean foldersOnly) {

		return navigationService.getDescendants(callContext, folderId, depth,
				filter, includeAllowableActions, includePathSegment,
				objectInfos, foldersOnly);
	}

	public ObjectData getFolderParent(CallContext callContext, String folderId,
			String filter, ObjectInfoHandler objectInfos) {

		return navigationService.getFolderParent(callContext, folderId, filter,
				objectInfos);
	}

	public List<ObjectParentData> getObjectParents(CallContext callContext,
			String objectId, String filter, Boolean includeAllowableActions,
			Boolean includeRelativePathSegment, ObjectInfoHandler objectInfos) {

		return navigationService.getObjectParents(callContext, objectId,
				filter, includeAllowableActions, includeRelativePathSegment,
				objectInfos);
	}

	public ObjectList getCheckedOutDocs(CallContext callContext,
			String folderId, String filter, String orderBy,
			Boolean includeAllowableActions,
			IncludeRelationships includeRelationships, String renditionFilter,
			BigInteger maxItems, BigInteger skipCount, ExtensionsData extension) {

		return navigationService.getCheckedOutDocs(callContext, folderId,
				filter, orderBy, includeAllowableActions, includeRelationships,
				renditionFilter, maxItems, skipCount, extension);
	}

	// --- Repository Service

	public boolean hasThisRepositoryId(String repositoryId) {
		return repositoryService.hasThisRepositoryId(repositoryId);
	}

	public RepositoryInfo getRepositoryInfo() {
		return repositoryService.getRepositoryInfo();
	}

	public TypeDefinitionList getTypeChildren(CallContext callContext,
			String typeId, Boolean includePropertyDefinitions,
			BigInteger maxItems, BigInteger skipCount) {
		return repositoryService.getTypeChildren(callContext, typeId,
				includePropertyDefinitions, maxItems, skipCount);
	}

	public List<TypeDefinitionContainer> getTypeDescendants(
			CallContext callContext, String typeId, BigInteger depth,
			Boolean includePropertyDefinitions) {
		return repositoryService.getTypeDescendants(callContext, typeId, depth,
				includePropertyDefinitions);
	}

	public TypeDefinition getTypeDefinition(CallContext callContext,
			String typeId) {
		return repositoryService.getTypeDefinition(callContext, typeId);
	}

	// --- ACL Service
	public Acl getAcl(CallContext callContext, String objectId) {
		return aclService.getAcl(callContext, objectId);
	}

	public Acl applyAcl(CallContext callContext, String objectId, Acl aces,
			AclPropagation aclPropagation) {
		Acl newACL = null;
		try {
			newACL = aclService.applyAcl(callContext, objectId, aces,
					aclPropagation);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newACL;
	}

	// --- Discovery Service

	/**
	 * Executes a CMIS query statement against the contents of the repository.
	 * 
	 * TODO this should be replaced with other search engine like Lucene.
	 */
	public ObjectList query(CallContext callContext, String statement,
			Boolean searchAllVersions, Boolean includeAllowableActions,
			IncludeRelationships includeRelationships, String renditionFilter,
			BigInteger maxItems, BigInteger skipCount, ExtensionsData extension) {

		return discoveryService.query(repositoryService.getTypeManager(),
				callContext, repositoryService.getRepositoryInfo().getId(),
				statement, searchAllVersions, includeAllowableActions,
				includeRelationships, renditionFilter, maxItems, skipCount,
				extension);
	}

	/**
	 * TODO Not Yet Implemented
	 */
	public ObjectList getContentChanges(String repositoryId,
			Holder<String> changeLogToken, Boolean includeProperties,
			String filter, Boolean includePolicyIds, Boolean includeAcl,
			BigInteger maxItems, ExtensionsData extension) {

		return discoveryService.getContentChanges(repositoryId, changeLogToken,
				includeProperties, filter, includePolicyIds, includeAcl,
				maxItems, extension);
	}

	public void setAclService(ACLService aclService) {
		this.aclService = aclService;
	}

	public void setDiscoveryService(DiscoveryService discoveryService) {
		this.discoveryService = discoveryService;
	}

	public void setNavigationService(NavigationService navigationService) {
		this.navigationService = navigationService;
	}

	public void setObjectService(ObjectService objectService) {
		this.objectService = objectService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

}