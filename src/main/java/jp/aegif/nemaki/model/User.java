/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.model;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Nemaki user.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends Content {

	private static final long serialVersionUID = 4372606781795402581L;

	public User() {
	}

	public User(String id, String name, String firstName, String lastName,
			String email, String parentId, String path, String type,
			String passwordHash, String creator, GregorianCalendar created) {
		setId(id);
		setName(name);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setParentId(parentId);
		setPath(path);
		setType(type);
		setPasswordHash(passwordHash);
		setCreator(creator);
		setCreated(created);
	}

	/**
	 * User's email address.
	 */
	private String email;

	/**
	 * User's last name.
	 */
	private String lastName;

	/**
	 * User's first name.
	 */
	private String firstName;

	/**
	 * Hash of the user's Nemaki password, using the BCrypt one-way algorithm.
	 * 
	 * @see jp.aegif.nemaki.util.PasswordHasher
	 */
	private String passwordHash;

	/*
	 * Getters/Setters
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	@Override
	public String toString() {
		@SuppressWarnings("serial")
		Map<String, Object> m = new HashMap<String, Object>() {
			{
				put("id", getId());
				put("name", getName());
				put("type", getType());
				put("email", getEmail());
				put("lastName", getLastName());
				put("firstName", getFirstName());
				put("passwordHash", getPasswordHash());
			}
		};
		return m.toString();
	}

}
