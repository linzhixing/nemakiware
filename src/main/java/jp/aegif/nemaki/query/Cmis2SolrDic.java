package jp.aegif.nemaki.query;

public class Cmis2SolrDic{
	private final static String CMIS_ID = "cmis:id";
	private final static String CMIS_NAME = "cmis:name";
	private final static String CMIS_ATTACHMENT = "cmis:attachment";
	private final static String CMIS_ASPECTS = "cmis:aspects";	//TODO　なぜ複数形
	
	private final static String SOLR_ID = "id";
	private final static String SOLR_NAME = "name";
	private final static String SOLR_ATTACHMENT = "attachment";
	private final static String SOLR_ASPECTS = "aspects";
	
	
	//TODO エラー処理
	public static String getSolrFiledName(String solrFieldName){
		if(solrFieldName.equals(CMIS_ID)){
			return SOLR_ID;
		}else if(solrFieldName.equals(CMIS_NAME)){
			return SOLR_NAME;
		}else if(solrFieldName.equals(CMIS_ATTACHMENT)){
			return SOLR_ATTACHMENT;
		}else if(solrFieldName.equals(CMIS_ASPECTS)){
			return SOLR_ASPECTS;
		}else{
			return null;
		}
		

	}
	

	
}