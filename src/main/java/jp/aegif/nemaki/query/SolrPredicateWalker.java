package jp.aegif.nemaki.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.antlr.runtime.tree.Tree;
import org.apache.chemistry.opencmis.commons.definitions.PropertyDefinition;
import org.apache.chemistry.opencmis.commons.enums.Cardinality;
import org.apache.chemistry.opencmis.commons.enums.PropertyType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.apache.chemistry.opencmis.server.support.query.CalendarHelper;
import org.apache.chemistry.opencmis.server.support.query.CmisQlStrictLexer;
import org.apache.chemistry.opencmis.server.support.query.CmisSelector;
import org.apache.chemistry.opencmis.server.support.query.ColumnReference;
import org.apache.chemistry.opencmis.server.support.query.QueryObject;
import org.apache.chemistry.opencmis.server.support.query.TextSearchLexer;
import org.apache.commons.lang.StringUtils;



public class SolrPredicateWalker {
	
	private QueryObject queryObject;
	
	private final String SPACE = " ";
	private final String COLON = ":";
	private final String NOT = "NOT";
	private final String AND = "AND";
	private final String OR = "OR";
	
	private final String SOLR_TEXT = "text";
	
	
	//コンストラクタ
	public SolrPredicateWalker(QueryObject queryObject) {
		this.queryObject = queryObject;
	}
	
	
	public String walkPredicate(Tree node) {
		switch (node.getType()) {
		case CmisQlStrictLexer.NOT:
            return walkNot(node.getChild(0));
		
		case CmisQlStrictLexer.AND:
			return walkAnd(node.getChild(0), node.getChild(1));
	    case CmisQlStrictLexer.OR:
	        return walkOr(node.getChild(0), node.getChild(1));
	
		case CmisQlStrictLexer.EQ:
	        return walkEquals(node.getChild(0), node.getChild(1));
		case CmisQlStrictLexer.NEQ:
            return walkNotEquals(node.getChild(0), node.getChild(1));
            
		case CmisQlStrictLexer.GT:
	        return walkGreaterThan(node.getChild(0), node.getChild(1));
	    case CmisQlStrictLexer.GTEQ:
	        return walkGreaterOrEquals(node.getChild(0), node.getChild(1));
	    case CmisQlStrictLexer.LT:
	        return walkLessThan(node.getChild(0), node.getChild(1));
	    case CmisQlStrictLexer.LTEQ:
	        return walkLessOrEquals(node.getChild(0), node.getChild(1));
    
	    case CmisQlStrictLexer.IN:
            return walkIn(node.getChild(0), node.getChild(1));
        case CmisQlStrictLexer.NOT_IN:
            return walkNotIn(node.getChild(0), node.getChild(1));

	        
	        
            
            
            
		case CmisQlStrictLexer.CONTAINS:
			if (node.getChildCount() == 1) {
				return walkContains(node, null, node.getChild(0));
	        } else {
	            return walkContains(node, node.getChild(0), node.getChild(1));
	        }


            
            
        default:
        	return null;
            
		}

	}
	
	public String walkNot(Tree node){
		
		String s;
		s = NOT + putInParens(walkPredicate(node));
		return s;
	}

	 public String walkAnd(Tree leftNode, Tree rightNode) {
		 String s;
		 //TODO 括弧で囲む必要があるかないか、最適化したい
		 s = putInParens(walkPredicate(leftNode)) + SPACE + AND + SPACE + putInParens(walkPredicate(rightNode));
		 return s;
	 }

	 public String walkOr(Tree leftNode, Tree rightNode) {
		 String s;
		 //ORは論理演算子の中でいちばん優先度が低いので、括弧処理を省いている
		 s = walkPredicate(leftNode) + walkPredicate(rightNode);
	     return s;
	 }

	
	public String walkEquals(Tree leftNode, Tree rightNode) {
		String s;
		HashMap<String,String> map = walkCompareInternal(leftNode, rightNode);
		s = map.get("field") + map.get("cond");	
		return s;
	}
	
	 public String walkNotEquals(Tree leftNode, Tree rightNode) {
	       String s;
	       s = NOT + putInParens(walkEquals(leftNode, rightNode));
	       return s;
	 }
	 
	 public String walkGreaterThan(Tree leftNode, Tree rightNode) {
		 String s;   
		 HashMap<String,String> map = walkCompareInternal(leftNode, rightNode);
		 s = map.get("field") + buildRangeString(true, true, map.get("cond"));
		 return s;
	 }

	    public String walkGreaterOrEquals(Tree leftNode, Tree rightNode) {
	    	 String s;   
			 HashMap<String,String> map = walkCompareInternal(leftNode, rightNode);
			 s = map.get("field") + buildRangeString(true, false, map.get("cond"));
			 return s;
	    }

	    public String walkLessThan(Tree leftNode, Tree rightNode) {
	    	 String s;   
			 HashMap<String,String> map = walkCompareInternal(leftNode, rightNode);
			 s = map.get("field") + buildRangeString(false, true, map.get("cond"));
			 return s;
	    }

	    public String walkLessOrEquals(Tree leftNode, Tree rightNode) {
	    	 String s;   
			 HashMap<String,String> map = walkCompareInternal(leftNode, rightNode);
			 s = map.get("field") + buildRangeString(false, false, map.get("cond"));
			 return s;
	    }
	    
	    //SolrにはIN構文がないので、Listの要素分だけ通常のクエリを結合したクエリを生成する
	    public String walkIn(Tree colNode, Tree listNode) {
	    	String s;
	    	
	    	List<String> statementList = new ArrayList<String>(); 
	    	
	    	String field = parseNodeToSolr(colNode);
	    	List list = (List) walkExpr(listNode);
	    	for(Object elm : list){
	    		statementList.add(field + elm.toString());
	    	}
	    	
	    	s = StringUtils.join(statementList, OR);
	    	
	        return s;
	    }

	    public String walkNotIn(Tree colNode, Tree listNode) {
	        String s;
	        s = NOT + putInParens(walkIn(colNode, listNode));
	        return s;
	    }
	    
	    
	    //LIKE以外も、walkの適用用件を満たしているかチェックするコードを真似るべきでは
	    public String walkLike(Tree colNode, Tree stringNode) {
	    	
				Object rVal = walkExpr(stringNode);
				if (!(rVal instanceof String)) {
					throw new IllegalStateException(
							"LIKE operator requires String literal on right hand side.");
				}

				ColumnReference colRef = getColumnReference(colNode);
				PropertyDefinition<?> pd = colRef.getPropertyDefinition();
				PropertyType propType = pd.getPropertyType();
				if (propType != PropertyType.STRING
						&& propType != PropertyType.HTML
						&& propType != PropertyType.ID
						&& propType != PropertyType.URI) {
					throw new IllegalStateException("Property type "
							+ propType.value() + " is not allowed FOR LIKE");
				}
				if (pd.getCardinality() != Cardinality.SINGLE) {
					throw new IllegalStateException(
							"LIKE is not allowed for multi-value properties ");
				}
				String propVal = null;
				// String propVal = (String) so.getProperties()
				// .get(colRef.getPropertyId()).getFirstValue();
				String pattern = translatePattern((String) rVal); // SQL to Solrのつもり
				// regex
				// syntax
				//Pattern p = Pattern.compile(pattern);
				
				String field = colRef.getName() + COLON;
				
				String s = field + pattern;
				return s;
	    }

	    public String walkNotLike(Tree colNode, Tree stringNode) {
	        String s = NOT + putInParens(walkLike(colNode, stringNode)); 
	        return s;
	    }


	    
	    

	 
	    public String walkContains(Tree opNode, Tree qualNode, Tree queryNode) {
	        if (qualNode != null) {
	            return walkSearchExpr(qualNode);	//TODO
	        }
	        return walkSearchExpr(queryNode);
	    }

	 
	 
	 ///////////////////////////////////
	 //searchExpre系の定義
	 ///////////////////////////////////
	 public String walkSearchExpr(Tree node) {
	        switch (node.getType()) {
	        case TextSearchLexer.TEXT_AND:
	            return walkTextAnd(node);
	        case TextSearchLexer.TEXT_OR:
	            return walkTextOr(node);
	        case TextSearchLexer.TEXT_MINUS:
	            return walkTextMinus(node);
	        case TextSearchLexer.TEXT_SEARCH_WORD_LIT:
	            return walkTextWord(node);
	        case TextSearchLexer.TEXT_SEARCH_PHRASE_STRING_LIT:
	            return walkTextPhrase(node);
	        default:
	            walkOtherExpr(node);
	            return null;
	        }
	    }

	 	protected String walkTextAnd(Tree node){
	 		String s;
	 		String op = SPACE + AND + SPACE;
	 		List<String> strings = new ArrayList<String>();
	 		
	 		for(int i=0;i<node.getChildCount();i++){
	 			Tree child = node.getChild(i);
	 			strings.add(walkSearchExpr(child));	//TODO 優先順位の問題のため括弧をつけておくべき？
	 		}
	 		
	 		s = StringUtils.join(strings, op);
	 		
	        return s;
	    }
	    
	    protected String walkTextOr(Tree node) {
	    	
	    	String s;
	 		String op = SPACE + OR + SPACE;
	 		List<String> strings = new ArrayList<String>();
	 		
	 		for(int i=0;i<node.getChildCount();i++){
	 			Tree child = node.getChild(i);
	 			strings.add(walkSearchExpr(child));	//TODO 括弧つけなくていい？
	 		}
	 		
	 		s = StringUtils.join(strings, op);
	 		
	        return s;
	    	
	    	
	    	
	    }
	    
	    protected String walkTextMinus(Tree node) {
	    	String s;
	    	
	    	Tree child = node.getChild(0);	//1個だけでいい？
	    	s = NOT + putInParens(walkSearchExpr(child));
	    	
	    	
	    	System.out.println("walkTextMinus");
	        return s;
	    }
	    
	    protected String walkTextWord(Tree node) {
	    	String s;
	    	
	    	String word = (String) node.toString();	//TODO nodeがStringであることをどこまで仕様として前提するか
	    	s= SOLR_TEXT + COLON + word;
	    	
	    	System.out.println("walkTextWord");
	        return s;
	    }
	    
	    protected String walkTextPhrase(Tree node) {
	    	
	    	String s;
	    	
	    	String phrase = (String) node.toString();	//TODO phraseの場合エスケープとかいる？
	    	s= SOLR_TEXT + COLON + phrase;
	    	
	    	System.out.println("walkTextWord");
	        return s;
	    	
	    	
	    	
	    }

	 
	    ///////////////////////////////////
	 
	
	
	///////////////////////////////////
	//Expre系の定義
	///////////////////////////////////
	public Object walkExpr(Tree node) {
        switch (node.getType()) {
        case CmisQlStrictLexer.BOOL_LIT:
            return walkBoolean(node);
        case CmisQlStrictLexer.NUM_LIT:
            return walkNumber(node);
        case CmisQlStrictLexer.STRING_LIT:
            return walkString(node);
        case CmisQlStrictLexer.TIME_LIT:
            return walkTimestamp(node);
        case CmisQlStrictLexer.IN_LIST:
            return walkList(node);
        case CmisQlStrictLexer.COL:
            return walkCol(node);	//いるの？
        case CmisQlStrictLexer.ID:
            return walkId(node);	//いるの？
        default:
            return walkOtherExpr(node);
        }
    }
	
	
	public Object walkBoolean(Tree node) {
	        String s = node.getText();
	        return Boolean.valueOf(s);
	}



	public Object walkNumber(Tree node) {
		String s = node.getText();
		if (s.contains(".") || s.contains("e") || s.contains("E")) {
			return Double.valueOf(s);
		} else {
			return Long.valueOf(s);
		}
	}

	public Object walkString(Tree node) {
		String s = node.getText();
		s = s.substring(1, s.length() - 1);
		s = s.replace("''", "'"); // unescape quotes
		return s;
	}

	public Object walkTimestamp(Tree node) {
		String s = node.getText();
		s = s.substring(s.indexOf('\'') + 1, s.length() - 1);
		return CalendarHelper.fromString(s);
	}

	public Object walkList(Tree node) {
        int n = node.getChildCount();
        List<Object> res = new ArrayList<Object>(n);
        for (int i = 0; i < n; i++) {
            res.add(walkExpr(node.getChild(i)));
        }
        return res;
    }
	
	public Object walkCol(Tree node) {
		return null;
	}

	public Object walkId(Tree node) {
		return null;
	}

	public Object walkOtherExpr(Tree node) {
        throw new CmisRuntimeException("Unknown node type: " + node.getType() + " (" + node.getText() + ")");
    }
	///////////////////////////////////
	
	
	
	
	
	private String putInParens(String str){
		return "(" + str + ")";
	}
	
	private String putInRangeParens(boolean exclusive,String str){
		if(exclusive){
			return "{" + str + "}";
		}else{
			return "[" + str + "]";
		}
	}
	
	
	/**
	 * Parse field name & condition value. Field name is formed for Solr query. 
	 * @param leftNode
	 * @param rightNode
	 * @return
	 */
	private HashMap<String, String> walkCompareInternal(Tree leftNode, Tree rightNode){
		HashMap<String,String> map = new HashMap<String, String>();
		String left = leftNode.toString();
		String right = rightNode.toString();
		
		map.put("field", Cmis2SolrDic.getSolrFiledName(left) + COLON);	//field名はあらかじめcolon付き
		map.put("cond", right);
		return map;
	}
	
	private String parseNodeToSolr(Tree node){
		String s = node.toString();
		
		//field名はあらかじめcolon付き
		return Cmis2SolrDic.getSolrFiledName(s) + COLON;
	}
	
	
	
	private String buildRangeString(Boolean greater, Boolean exclusive,String c){
		String r;
		if(greater){
			r = c + SPACE + "TO" + SPACE + "*";
		}else{
			r = "*" + SPACE + "TO" + SPACE + c;
		}
		return putInRangeParens(exclusive,r);
	}
	
	
	//TODO SQL to Javaのもの。SQL to Solrに書き直す必要あり。walkLike用。
	public static String translatePattern(String wildcardString) {
		int index = 0;
		int start = 0;
		StringBuffer res = new StringBuffer();

		while (index >= 0) {
			index = wildcardString.indexOf('%', start);
			if (index < 0) {
				res.append(wildcardString.substring(start));
			} else if (index == 0 || index > 0
					&& wildcardString.charAt(index - 1) != '\\') {
				res.append(wildcardString.substring(start, index));
				res.append(".*");
			} else {
				res.append(wildcardString.substring(start, index + 1));
			}
			start = index + 1;
		}
		wildcardString = res.toString();

		index = 0;
		start = 0;
		res = new StringBuffer();

		while (index >= 0) {
			index = wildcardString.indexOf('_', start);
			if (index < 0) {
				res.append(wildcardString.substring(start));
			} else if (index == 0 || index > 0
					&& wildcardString.charAt(index - 1) != '\\') {
				res.append(wildcardString.substring(start, index));
				res.append("?");	//SQL to Javaのときは "." にリプレースされていた
			} else {
				res.append(wildcardString.substring(start, index + 1));
			}
			start = index + 1;
		}
		return res.toString();
	}
	
	private ColumnReference getColumnReference(Tree columnNode) {
		CmisSelector sel = queryObject.getColumnReference(columnNode
				.getTokenStartIndex());
		if (null == sel) {
			throw new IllegalStateException("Unknown property query name "
					+ columnNode.getChild(0));
		} else if (sel instanceof ColumnReference) {
			return (ColumnReference) sel;
		} else {
			throw new IllegalStateException(
					"Unexpected numerical value function in where clause");
		}
	}
	
	
	
}
