/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.query;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.repository.TypeManager;
import jp.aegif.nemaki.service.NodeService;

import org.antlr.runtime.tree.Tree;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.ObjectList;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.definitions.PropertyDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.Cardinality;
import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships;
import org.apache.chemistry.opencmis.commons.enums.PropertyType;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectListImpl;
import org.apache.chemistry.opencmis.server.support.query.AbstractPredicateWalker;
import org.apache.chemistry.opencmis.server.support.query.CmisQueryWalker;
import org.apache.chemistry.opencmis.server.support.query.CmisSelector;
import org.apache.chemistry.opencmis.server.support.query.ColumnReference;
import org.apache.chemistry.opencmis.server.support.query.QueryObject;
import org.apache.chemistry.opencmis.server.support.query.QueryUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * TODO This class is under construction.
 * 
 * @see org.apache.chemistry.opencmis.inmemory.query.InMemoryQueryProcessor
 */
public class NemakiQueryProcessor implements QueryProcessor {

	private static final Log log = LogFactory
			.getLog(NemakiQueryProcessor.class);
	private AbstractPredicateWalker NemakiWhereClauseWalker;

	private NodeService nodeService;

	private QueryObject queryObject;
	private Tree whereTree;
	private List<ObjectData> matches = new ArrayList<ObjectData>();

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public NemakiQueryProcessor() {
	}

	public void setNemakiWhereClauseWalker(
			AbstractPredicateWalker nemakiWhereClauseWalker) {
		NemakiWhereClauseWalker = nemakiWhereClauseWalker;
	}

	public ObjectList query(TypeManager typeManager, String username,
			String id, String statement, Boolean searchAllVersions,
			Boolean includeAllowableActions,
			IncludeRelationships includeRelationships, String renditionFilter,
			BigInteger maxItems, BigInteger skipCount) {

		queryObject = new QueryObject(typeManager);
		log.debug("process query: " + statement);
		log.debug("column refs: " + queryObject.getColumnReferences());
		log.debug("aaaa refs: " + queryObject.getTypeQueryName("cmis"));
		log.debug("bbbb refs: " + queryObject.getMainTypeAlias());
		log.debug("cccc refs: " + queryObject.getMainFromName());
		log.debug("dddd refs: " + queryObject.getSelectReferences());
		log.debug("eeee refs: "
				+ queryObject.getTypeDefinitionFromQueryName("cmis:document"));
		log.debug("fff refs: "
				+ queryObject.getTypeDefinitionFromQueryName("cmis:folder"));
		log.debug("gggg refs: " + queryObject.getTypes());
		log.debug("hhhh refs: " + queryObject.getWhereReferences());
		processQueryAndCatchExc(statement);

		// TODO to get all ids may have problem in performance.
		List<String> ids = nodeService.allIds();
		log.debug("all ids: " + ids.size());
		for (String objectId : ids) {
			Content content = nodeService.get(Content.class, objectId);
			match(content, username, searchAllVersions == null ? true
					: searchAllVersions.booleanValue());
		}

		ObjectList objList = new ObjectListImpl();
		// ObjectList objList = buildResultList(typeManager, username,
		// includeAllowableActions, includeRelationships, renditionFilter,
		// maxItems, skipCount);
		log.debug("Query matching objects: " + objList.getNumItems());
		return objList;
	}

	private void match(Content content, String username,
			boolean searchAllVersions) {
		String queryName = queryObject.getTypes().values().iterator().next();
		TypeDefinition td = queryObject
				.getTypeDefinitionFromQueryName(queryName);

		// set true if NOT content/document/folder/group/user
		boolean skip = false;

		boolean typeMatches = typeMatches(td, content);
		if (typeMatches && !skip) {
			evalWhereTree(whereTree, username, content);
		}
	}

	private boolean typeMatches(TypeDefinition td, Content content) {
		String typeId = content.getType();
		while (typeId != null) {
			if (typeId.equals(td.getId())) {
				return true;
			}
			// check parent type
			TypeDefinition parentTD = queryObject.getParentType(typeId);
			typeId = parentTD == null ? null : parentTD.getId();
		}
		return false;
	}

	private void processQueryAndCatchExc(String statement) {
		QueryUtil util = new QueryUtil();
		log.debug("aaaaaa");
		CmisQueryWalker walker = null;
		try {
			walker = util.traverseStatementAndCatchExc(statement, queryObject,
					null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("bbbbbbbbb");
		whereTree = walker.getWherePredicateTree();
		log.debug("walker set tree: " + walker.getWherePredicateTree());
	}

	private void evalWhereTree(Tree node, String username, Content content) {
		boolean match = true;
		if (null != node) {
			match = evalWhereNode(content, username, node);
		}
		if (match) {
			// matches.add(content); // add to list
		}
	}

	private boolean evalWhereNode(Content content, String username, Tree node) {
		return new NemakiWhereClauseWalker(content, username)
				.walkPredicate(node);
	}

	public class NemakiWhereClauseWalker extends AbstractPredicateWalker {
		protected final Content content;
		protected final String user;

		public NemakiWhereClauseWalker(Content content, String user) {
			this.content = content;
			this.user = user;
		}

		@Override
		public Boolean walkNot(Tree opNode, Tree node) {
			boolean matches = walkPredicate(node);
			return !matches;
		}

		@Override
		public Boolean walkAnd(Tree opNode, Tree leftNode, Tree rightNode) {
			boolean matches1 = walkPredicate(leftNode);
			boolean matches2 = walkPredicate(rightNode);
			return matches1 && matches2;
		}

		@Override
		public Boolean walkOr(Tree opNode, Tree leftNode, Tree rightNode) {
			boolean matches1 = walkPredicate(leftNode);
			boolean matches2 = walkPredicate(rightNode);
			return matches1 || matches2;
		}

		@Override
		public Boolean walkEquals(Tree opNode, Tree leftNode, Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp == 0;
		}

		@Override
		public Boolean walkNotEquals(Tree opNode, Tree leftNode, Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp != 0;
		}

		@Override
		public Boolean walkGreaterThan(Tree opNode, Tree leftNode,
				Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp > 0;
		}

		@Override
		public Boolean walkGreaterOrEquals(Tree opNode, Tree leftNode,
				Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp >= 0;
		}

		@Override
		public Boolean walkLessThan(Tree opNode, Tree leftNode, Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp < 0;
		}

		@Override
		public Boolean walkLessOrEquals(Tree opNode, Tree leftNode,
				Tree rightNode) {
			Integer cmp = compareTo(leftNode, rightNode);
			return cmp == null ? false : cmp <= 0;
		}

		@Override
		public Boolean walkIn(Tree opNode, Tree colNode, Tree listNode) {
			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// PropertyData<?> lVal = so.getProperties().get(
			// colRef.getPropertyId());
			PropertyData<?> lVal = null;
			List<Object> literals = onLiteralList(listNode);
			if (pd.getCardinality() != Cardinality.SINGLE) {
				throw new IllegalStateException(
						"Operator IN only is allowed on single-value properties ");
			} else if (lVal == null) {
				return false;
			} else {
				Object prop = lVal.getFirstValue();
				return literals.contains(prop);
			}
		}

		@Override
		public Boolean walkNotIn(Tree opNode, Tree colNode, Tree listNode) {
			// Note just return !walkIn(node, colNode, listNode) is wrong,
			// because
			// then it evaluates to true for null values (not set properties).
			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// PropertyData<?> lVal = so.getProperties().get(
			// colRef.getPropertyId());
			PropertyData<?> lVal = null;
			List<Object> literals = onLiteralList(listNode);
			if (pd.getCardinality() != Cardinality.SINGLE) {
				throw new IllegalStateException(
						"Operator IN only is allowed on single-value properties ");
			} else if (lVal == null) {
				return false;
			} else {
				Object prop = lVal.getFirstValue();
				return !literals.contains(prop);
			}
		}

		@Override
		public Boolean walkInAny(Tree opNode, Tree colNode, Tree listNode) {
			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// PropertyData<?> lVal = so.getProperties().get(
			// colRef.getPropertyId());
			PropertyData<?> lVal = null;
			List<Object> literals = onLiteralList(listNode);
			if (pd.getCardinality() != Cardinality.MULTI) {
				throw new IllegalStateException(
						"Operator ANY...IN only is allowed on multi-value properties ");
			} else if (lVal == null) {
				return false;
			} else {
				List<?> props = lVal.getValues();
				for (Object prop : props) {
					log.debug("comparing with: " + prop);
					if (literals.contains(prop)) {
						return true;
					}
				}
				return false;
			}
		}

		@Override
		public Boolean walkNotInAny(Tree opNode, Tree colNode, Tree listNode) {
			// Note just return !walkNotInAny(node, colNode, listNode) is
			// wrong, because
			// then it evaluates to true for null values (not set properties).
			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// PropertyData<?> lVal = so.getProperties().get(
			// colRef.getPropertyId());
			PropertyData<?> lVal = null;
			List<Object> literals = onLiteralList(listNode);
			if (pd.getCardinality() != Cardinality.MULTI) {
				throw new IllegalStateException(
						"Operator ANY...IN only is allowed on multi-value properties ");
			} else if (lVal == null) {
				return false;
			} else {
				List<?> props = lVal.getValues();
				for (Object prop : props) {
					log.debug("comparing with: " + prop);
					if (literals.contains(prop)) {
						return false;
					}
				}
				return true;
			}
		}

		@Override
		public Boolean walkEqAny(Tree opNode, Tree literalNode, Tree colNode) {
			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// PropertyData<?> lVal = so.getProperties().get(
			// colRef.getPropertyId());
			PropertyData<?> lVal = null;
			Object literal = walkExpr(literalNode);
			if (pd.getCardinality() != Cardinality.MULTI) {
				throw new IllegalStateException(
						"Operator = ANY only is allowed on multi-value properties ");
			} else if (lVal == null) {
				return false;
			} else {
				List<?> props = lVal.getValues();
				return props.contains(literal);
			}
		}

		@Override
		public Boolean walkIsNull(Tree opNode, Tree colNode) {
			Object propVal = getPropertyValue(colNode, content);
			return propVal == null;
		}

		@Override
		public Boolean walkIsNotNull(Tree opNode, Tree colNode) {
			Object propVal = getPropertyValue(colNode, content);
			return propVal != null;
		}

		@Override
		public Boolean walkLike(Tree opNode, Tree colNode, Tree stringNode) {
			Object rVal = walkExpr(stringNode);
			if (!(rVal instanceof String)) {
				throw new IllegalStateException(
						"LIKE operator requires String literal on right hand side.");
			}

			ColumnReference colRef = getColumnReference(colNode);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			PropertyType propType = pd.getPropertyType();
			if (propType != PropertyType.STRING
					&& propType != PropertyType.HTML
					&& propType != PropertyType.ID
					&& propType != PropertyType.URI) {
				throw new IllegalStateException("Property type "
						+ propType.value() + " is not allowed FOR LIKE");
			}
			if (pd.getCardinality() != Cardinality.SINGLE) {
				throw new IllegalStateException(
						"LIKE is not allowed for multi-value properties ");
			}
			String propVal = null;
			// String propVal = (String) so.getProperties()
			// .get(colRef.getPropertyId()).getFirstValue();
			String pattern = translatePattern((String) rVal); // SQL to Java
			// regex
			// syntax
			Pattern p = Pattern.compile(pattern);
			return p.matcher(propVal).matches();
		}

		@Override
		public Boolean walkNotLike(Tree opNode, Tree colNode, Tree stringNode) {
			return !walkLike(opNode, colNode, stringNode);
		}

		@Override
		public Boolean walkInFolder(Tree opNode, Tree qualNode, Tree paramNode) {
			if (null != qualNode) {
				getTableReference(qualNode);
				// just for error checking we do not evaluate this, there is
				// only one from without join support
			}
			Object lit = walkExpr(paramNode);
			if (!(lit instanceof String)) {
				throw new IllegalStateException(
						"Folder id in IN_FOLDER must be of type String");
			}
			String folderId = (String) lit;

			// check if object is in folder
			// if (so instanceof Filing) {
			// return hasParent((Filing) so, folderId, user);
			// } else {
			// return false;
			// }
			return false;
		}

		@Override
		public Boolean walkInTree(Tree opNode, Tree qualNode, Tree paramNode) {
			if (null != qualNode) {
				getTableReference(qualNode);
				// just for error checking we do not evaluate this, there is
				// only one from without join support
			}
			Object lit = walkExpr(paramNode);
			if (!(lit instanceof String)) {
				throw new IllegalStateException(
						"Folder id in IN_FOLDER must be of type String");
			}
			String folderId = (String) lit;

			// check if object is in folder
			// if (so instanceof Filing) {
			// return hasAncestor((Filing) so, folderId, user);
			// } else {
			// return false;
			// }
			return false;
		}

		@SuppressWarnings("unchecked")
		public List<Object> onLiteralList(Tree node) {
			return (List<Object>) walkExpr(node);
		}

		protected Integer compareTo(Tree leftChild, Tree rightChild) {
			Object rVal = walkExpr(rightChild);

			ColumnReference colRef = getColumnReference(leftChild);
			PropertyDefinition<?> pd = colRef.getPropertyDefinition();
			// Object val = PropertyUtil.getProperty(so,
			// colRef.getPropertyId());
			Object val = null;
			if (val == null) {
				return null;
			}
			if (val instanceof List<?>) {
				throw new IllegalStateException(
						"You can't query operators <, <=, ==, !=, >=, > on multi-value properties ");
			} else {
				return NemakiQueryProcessor.this.compareTo(pd, val, rVal);
			}
		}

	}

	public static String translatePattern(String wildcardString) {
		int index = 0;
		int start = 0;
		StringBuffer res = new StringBuffer();

		while (index >= 0) {
			index = wildcardString.indexOf('%', start);
			if (index < 0) {
				res.append(wildcardString.substring(start));
			} else if (index == 0 || index > 0
					&& wildcardString.charAt(index - 1) != '\\') {
				res.append(wildcardString.substring(start, index));
				res.append(".*");
			} else {
				res.append(wildcardString.substring(start, index + 1));
			}
			start = index + 1;
		}
		wildcardString = res.toString();

		index = 0;
		start = 0;
		res = new StringBuffer();

		while (index >= 0) {
			index = wildcardString.indexOf('_', start);
			if (index < 0) {
				res.append(wildcardString.substring(start));
			} else if (index == 0 || index > 0
					&& wildcardString.charAt(index - 1) != '\\') {
				res.append(wildcardString.substring(start, index));
				res.append(".");
			} else {
				res.append(wildcardString.substring(start, index + 1));
			}
			start = index + 1;
		}
		return res.toString();
	}

	protected int compareTo(PropertyDefinition<?> td, Object lValue, Object rVal) {
		switch (td.getPropertyType()) {
		case BOOLEAN:
			if (rVal instanceof Boolean) {
				return ((Boolean) lValue).compareTo((Boolean) rVal);
			} else {
				throwIncompatibleTypesException(lValue, rVal);
			}
			break;
		case INTEGER: {
			Long lLongValue = ((BigInteger) lValue).longValue();
			if (rVal instanceof Long) {
				return (lLongValue).compareTo((Long) rVal);
			} else if (rVal instanceof Double) {
				return Double.valueOf(((Integer) lValue).doubleValue())
						.compareTo((Double) rVal);
			} else {
				throwIncompatibleTypesException(lValue, rVal);
			}
			break;
		}
		case DATETIME:
			if (rVal instanceof GregorianCalendar) {
				// LOG.debug("left:" +
				// CalendarHelper.toString((GregorianCalendar)lValue) +
				// " right: " +
				// CalendarHelper.toString((GregorianCalendar)rVal));
				return ((GregorianCalendar) lValue)
						.compareTo((GregorianCalendar) rVal);
			} else {
				throwIncompatibleTypesException(lValue, rVal);
			}
			break;
		case DECIMAL: {
			Double lDoubleValue = ((BigDecimal) lValue).doubleValue();
			if (rVal instanceof Double) {
				return lDoubleValue.compareTo((Double) rVal);
			} else if (rVal instanceof Long) {
				return Double.valueOf(((Integer) lValue).doubleValue())
						.compareTo(((Long) rVal).doubleValue());
			} else {
				throwIncompatibleTypesException(lValue, rVal);
			}
			break;
		}
		case HTML:
		case STRING:
		case URI:
		case ID:
			if (rVal instanceof String) {
				log.debug("compare strings: " + lValue + " with " + rVal);
				return ((String) lValue).compareTo((String) rVal);
			} else {
				throwIncompatibleTypesException(lValue, rVal);
			}
			break;
		}
		return 0;
	}

	private static void throwIncompatibleTypesException(Object o1, Object o2) {
		throw new IllegalArgumentException("Incompatible Types to compare: "
				+ o1 + " and " + o2);
	}

	private ColumnReference getColumnReference(Tree columnNode) {
		CmisSelector sel = queryObject.getColumnReference(columnNode
				.getTokenStartIndex());
		if (null == sel) {
			throw new IllegalStateException("Unknown property query name "
					+ columnNode.getChild(0));
		} else if (sel instanceof ColumnReference) {
			return (ColumnReference) sel;
		} else {
			throw new IllegalStateException(
					"Unexpected numerical value function in where clause");
		}
	}

	private String getTableReference(Tree tableNode) {
		String typeQueryName = queryObject
				.getTypeQueryName(tableNode.getText());
		if (null == typeQueryName) {
			throw new IllegalStateException(
					"Inavlid type in IN_FOLDER() or IN_TREE(), must be in FROM list: "
							+ tableNode.getText());
		}
		return typeQueryName;
	}

	private Object getPropertyValue(Tree columnNode, Content content) {
		ColumnReference colRef = getColumnReference(columnNode);
		PropertyDefinition<?> pd = colRef.getPropertyDefinition();
		// PropertyData<?> lVal =
		// so.getProperties().get(colRef.getPropertyId());
		PropertyData<?> lVal = null;
		if (null == lVal) {
			return null;
		} else {
			if (pd.getCardinality() == Cardinality.SINGLE) {
				return lVal.getFirstValue();
			} else {
				return lVal.getValues();
			}
		}
	}

}
