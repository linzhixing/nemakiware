package jp.aegif.nemaki.query;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Document;
import jp.aegif.nemaki.repository.TypeManager;
import jp.aegif.nemaki.service.NemakiCmisService;
import jp.aegif.nemaki.service.NodeService;
import jp.aegif.nemaki.service.PermissionService;
import jp.aegif.nemaki.service.UserGroupService;
import jp.aegif.nemaki.service.impl.NodeServiceImpl;

import org.antlr.runtime.tree.Tree;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.ObjectList;
import org.apache.chemistry.opencmis.commons.data.PermissionMapping;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.PropertiesImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.PropertyStringImpl;
import org.apache.chemistry.opencmis.server.support.query.AbstractPredicateWalker;
import org.apache.chemistry.opencmis.server.support.query.CmisQlExtParser_CmisBaseGrammar.predicate_return;
import org.apache.chemistry.opencmis.server.support.query.CmisQlStrictLexer;
import org.apache.chemistry.opencmis.server.support.query.CmisQlStrictParser;
import org.apache.chemistry.opencmis.server.support.query.CmisQueryWalker;
import org.apache.chemistry.opencmis.server.support.query.QueryObject;
import org.apache.chemistry.opencmis.server.support.query.QueryUtil;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.SolrParams;


public class SolrQueryProcessor implements QueryProcessor{

	private NodeService nodeService;
	private PermissionService permissionService;
	private UserGroupService userGroupService;
	
	private SolrServer solrServer;
	private final String solrUrl = "http://localhost:8983/solr/"; 
	
	private QueryObject queryObject;
	private Tree whereTree;
	private List<ObjectData> matches = new ArrayList<ObjectData>();
	
	
	public SolrQueryProcessor() {
		
		solrServer = new HttpSolrServer(solrUrl);
		
		
	}
	
	
	public ObjectList query(TypeManager typeManager, String username,
			String id, String statement, Boolean searchAllVersions,
			Boolean includeAllowableActions,
			IncludeRelationships includeRelationships, String renditionFilter,
			BigInteger maxItems, BigInteger skipCount) {
		
		ObjectListImpl objectList = new ObjectListImpl();
		objectList.setObjects(new ArrayList<ObjectData>());

		queryObject = new QueryObject(typeManager);
		
		QueryUtil util = new QueryUtil();
		CmisQueryWalker walker = null;
		try {

			
		
			walker = util.traverseStatementAndCatchExc(statement, queryObject,
					null);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		String queryName = queryObject.getTypes().values().iterator().next();
		TypeDefinition td = queryObject
				.getTypeDefinitionFromQueryName(queryName);
	
		
		whereTree = walker.getWherePredicateTree();
		
		SolrPredicateWalker solrPredicateWalker = new SolrPredicateWalker(queryObject);
		String s = solrPredicateWalker.walkPredicate(whereTree);
		
		//NemakiWalker nemakiWalker = new NemakiWalker();
		//nemakiWalker.walkPredicate(whereTree);
		
		//Solr connection
		SolrQuery query = new SolrQuery();
		query.setQuery(s);
		
		QueryResponse resp = null;
		try {
			resp = solrServer.query(query);
		} catch (SolrServerException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		if(resp != null & resp.getResults().getNumFound() != 0){
			SolrDocumentList docs = resp.getResults();
			
			List<ObjectData> dataList = new ArrayList<ObjectData>();
			
			
			for(SolrDocument doc : docs){
				String docId = (String) doc.getFieldValue("id");	//Dictionary化する
				Content c = nodeService.get(Content.class, docId);
				
				PropertiesImpl props = new PropertiesImpl();
				props.addProperty(new PropertyStringImpl("id", c.getId()));
				props.addProperty(new PropertyStringImpl("type", c.getType()));
				props.addProperty(new PropertyStringImpl("name", c.getName()));
				ObjectDataImpl data = new ObjectDataImpl();
				data.setProperties(props);
				dataList.add(data);
			}
			
			objectList.getObjects().addAll(dataList);
			
		}
	
		
		return objectList;
	}

	
	

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	



}