/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Permission;
import jp.aegif.nemaki.model.Role;

import org.apache.chemistry.opencmis.commons.data.PermissionMapping;
import org.apache.chemistry.opencmis.commons.definitions.PermissionDefinition;
import org.apache.chemistry.opencmis.commons.enums.Action;

/**
 * Permission Service interface.
 */
public interface PermissionService {

	/*
	 * Permissions.
	 */
	public static final String CMIS_READ_PERMISSION = "cmis:read";
	public static final String CMIS_WRITE_PERMISSION = "cmis:write";
	public static final String CMIS_ALL_PERMISSION = "cmis:all";

	/**
	 * Get default permission for content to be created.
	 */
	public Permission getDefaultPermission(String username);

	/**
	 * Get the list of existing permissions. For instance: read, write, all
	 */
	public List<PermissionDefinition> getPermissionDefinitions();

	/**
	 * Mapping permission group to elemental permission (repository static)
	 */
	public Map<String, PermissionMapping> getPermissionMap();

	/**
	 * Filter contents using permission entries
	 * 
	 * @param username
	 *            user who is doing operation <br/>
	 *            (e.g. admin)
	 * @param operation
	 *            also represents permission mapping key <br/>
	 *            (e.g. CAN_CREATE_DOCUMENT_FOLDER)
	 * @param contents
	 *            original contents which is got from DB
	 */
	public List<Content> getFiltered(String username, String operation,
			List<Content> contents);

	/**
	 * create actionset for the role
	 * 
	 * @param role
	 * @param isFolder
	 * @param isRoot
	 * @return TODO change to use DI for performance
	 */
	public Set<Action> getAllowableActionset(Role role, boolean isFolder,
			boolean isRoot);

}
