package jp.aegif.nemaki.service;

import java.math.BigInteger;
import java.util.List;

import jp.aegif.nemaki.model.Content;

import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.ExtensionsData;
import org.apache.chemistry.opencmis.commons.data.FailedToDeleteData;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.Properties;
import org.apache.chemistry.opencmis.commons.data.RenditionData;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.ObjectInfoHandler;
import org.apache.chemistry.opencmis.commons.spi.Holder;

public interface ObjectService {

	/**
	 * Creates a new document, folder or policy. The property
	 * "cmis:objectTypeId" defines the type and implicitly the base type.
	 * 
	 * @return object representing created document.
	 */
	public abstract ObjectData create(CallContext callContext,
			Properties properties, String folderId,
			ContentStream contentStream, VersioningState versioningState,
			ObjectInfoHandler objectInfos);

	/**
	 * Sets the content stream for the specified document object.
	 */
	public abstract void setContentStream(CallContext callContext,
			Holder<String> objectId, boolean overwriteFlag,
			ContentStream contentStream);

	/**
	 * Deletes the specified folder object and all of its child- and
	 * descendant-objects.
	 * 
	 * TODO Not Yet Implemented
	 */
	public abstract FailedToDeleteData deleteTree(CallContext callContext,
			String folderId, Boolean continueOnFailure);

	/**
	 * Deletes object. Attachments of the object get deleted too.
	 * 
	 * @param objectId
	 *            id of the object to be deleted.
	 */
	public abstract void deleteObject(CallContext callContext, String objectId);

	/**
	 * Moves the specified file-able object from one folder to another.
	 * 
	 * TODO Not Yet Implemented
	 */
	public abstract void moveObject(CallContext callContext,
			Holder<String> objectId, String targetFolderId,
			NemakiCmisService couchCmisService);

	/**
	 * Gets the list of associated renditions for the specified object. Only
	 * rendition attributes are returned, not rendition stream. No renditions,
	 * so empty.
	 * 
	 * TODO Not Yet Implemented
	 */
	public abstract List<RenditionData> getRenditions(CallContext callContext,
			String objectId, String renditionFilter, BigInteger maxItems,
			BigInteger skipCount, ExtensionsData extension);

	/**
	 * Updates properties of the object. Doing so also updates the
	 * "last modified" date. Custom properties(Aspect) is passed as
	 * CmisExtensionElement
	 */
	public abstract void updateProperties(CallContext callContext,
			Holder<String> objectId, Properties properties,
			NemakiCmisService couchCmisService);

	/**
	 * Gets the content stream for the specified document object, or gets a
	 * rendition stream for a specified rendition of a document or folder
	 * object.
	 */
	public abstract ContentStream getContentStream(CallContext callContext,
			String objectId, BigInteger offset, BigInteger length);

	/**
	 * Gets the specified information for the object specified by path.
	 */
	public abstract ObjectData getObjectByPath(CallContext callContext,
			String path, String filter, Boolean includeAllowableActions,
			Boolean includeAcl, ObjectInfoHandler objectInfos);

	/**
	 * Gets the specified information for the object specified by id.
	 */
	public abstract ObjectData getObject(CallContext callContext,
			String objectId, String versionServicesId, String filter,
			Boolean includeAllowableActions, Boolean includeAcl,
			ObjectInfoHandler objectInfos);

	/**
	 * Gets the list of allowable actions for an object.
	 * 
	 * TODO Not Yet Implemented
	 */
	public abstract AllowableActions getAllowableActions(
			CallContext callContext, String objectId);

	/**
	 * Creates a folder object of the specified type (given by the
	 * cmis:objectTypeId property) in the specified location.
	 */
	public abstract String createFolder(CallContext callContext,
			Properties properties, String folderId);

	/**
	 * Creates a document object as a copy of the given source document in the
	 * (optionally) specified location.
	 * 
	 * TODO Not Yet Implemented
	 */
	public abstract String createDocumentFromSource(CallContext callContext,
			String sourceId, Properties properties, String folderId,
			VersioningState versioningState);

	/**
	 * Creates a document object of the specified type (given by the
	 * cmis:objectTypeId property) in the (optionally) specified location.
	 * 
	 * @return id of created document.
	 */
	public abstract String createDocument(CallContext callContext,
			Properties properties, String folderId,
			ContentStream contentStream, VersioningState versioningState);

	public abstract ObjectData compileObjectType(CallContext context,
			Content content, String filter, boolean includeAllowableActions,
			boolean includeAcl, ObjectInfoHandler objectInfos);
}
