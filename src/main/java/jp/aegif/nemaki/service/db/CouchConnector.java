/**

 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.db;

import java.util.ArrayList;
import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Connection class for CouchDB
 */

public class CouchConnector {

	/**
	 * CouchDB connector.
	 */
	private CouchDbConnector connector;

	/**
	 * Host, for instance: 127.0.0.1
	 */
	private String host;

	/**
	 * Repository id, for instance: books
	 */
	@Value("${repositoryId}")
	private String[] repositoryId;

	/**
	 * Max connections, for instance: 40
	 */
	private int maxConnections;


	/**
	 * Initialize this class with host, maxConnections.
	 */
	public void init() {
		HttpClient httpClient = new StdHttpClient.Builder().host(host)
				.maxConnections(maxConnections).build();
		CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);

		//aegif
		String repo = "";		
		try{
			repo = repositoryId[0];
		}catch(Exception ex){
			ex.printStackTrace();
		}
		connector = dbInstance.createConnector(repo, true);
	}

	public CouchDbConnector getConnection() {
		return connector;
	}

	public void setRepositoryId(String[] repositoryId) {	
		this.repositoryId = repositoryId;
	}
	
	public void setHost(String host) {
		this.host = host;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

}
