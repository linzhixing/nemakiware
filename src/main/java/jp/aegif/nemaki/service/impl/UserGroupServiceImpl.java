/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.aegif.nemaki.model.Group;
import jp.aegif.nemaki.model.User;
import jp.aegif.nemaki.service.DaoService;
import jp.aegif.nemaki.service.UserGroupService;

/**
 * UserGroup Service implementation.
 * 
 */
public class UserGroupServiceImpl implements UserGroupService {

	private DaoService daoService;
	private List<Group> groups = new ArrayList<Group>();
	private List<User> users = new ArrayList<User>();

	public void init() {
		users = daoService.getUsers();
		groups = daoService.getGroups();
	}

	public List<User> getUsers() {
		//refresh to cope with new user without restarting the server
		users = daoService.getUsers();
		return users;
	}

	public List<Group> getGroups() {
		//refresh to cope with new group without restarting the server
		groups = daoService.getGroups();
		return groups;
	}

	public Set<String> getGroupsContainingUser(String username) {
		Set<String> groupIds = new HashSet<String>();
		for (Group g : groups) {
			if (g.getUsers().contains(username))
				groupIds.add(g.getId());
		}
		groupIds.add(UserGroupService.GROUP_EVERYONE);
		return groupIds;
	}

	public User getUserByName(String username) {
		return daoService.getUserByName(username);
	}

	public Group getGroupById(String groupId) {
		return daoService.getGroupById(groupId);
	}

	public Set<Group> getGroupsIncludingParents(Set<String> groupIds) {
		Set<Group> allGroups = new HashSet<Group>();
		for (String groupId : groupIds) {
			// Add this group
			Group group = getGroupById(groupId);
			allGroups.add(group);
			// Add parents of this group
			while (group.getParentId() != null) {
				group = getGroupById(group.getParentId());
				allGroups.add(group);
			}
		}
		return allGroups;
	}

	public void setDaoService(DaoService daoService) {
		this.daoService = daoService;
	}

	public User getUserById(String Id) {
		return (User) daoService.get(User.class, Id);
	}

	public void createUser(User user) {
		daoService.create(user);
	}

	public void updateUser(User user) {
		daoService.update(user);
	}

	public void deleteUser(String userId) {
		daoService.deleteUser(User.class, userId);
	}

	public void createGroup(Group group) {
		daoService.create(group);
	}

	public void updateGroup(Group group) {
		daoService.update(group);
	}

	public void deleteGroup(String groupId) {
		daoService.deleteGroup(Group.class, groupId);
	}
}