/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import jp.aegif.nemaki.api.resources.ResourceBase;
import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Document;
import jp.aegif.nemaki.model.Folder;
import jp.aegif.nemaki.model.NemakiAttachment;
import jp.aegif.nemaki.model.User;
import jp.aegif.nemaki.service.DaoService;
import jp.aegif.nemaki.service.NodeService;

import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.ektorp.AttachmentInputStream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Node Service implementation.
 * 
 */
public class NodeServiceImpl implements NodeService {

	private DaoService daoService;

	/**
	 * Get one piece of CMIS content.
	 * 
	 * @clazz Type of CMIS content expected, must inherit from Content.
	 */
	public Content get(Class<?> clazz, String objectId) {
		return daoService.get(clazz, objectId);
	}

	/**
	 * Create one content.
	 */
	public void create(Content content) {
		daoService.create(content);
	}

	/**
	 * Update one content.
	 */
	public void update(Content content) {
		daoService.update(content);
	}

	/**
	 * Delete a Content.
	 */
	public void delete(Class<Content> clazz, String objectId) {
		daoService.delete(clazz, objectId);
	}

	/**
	 * Get the list of all users.
	 */
	public List<User> getUsers() {
		return daoService.getUsers();
	}

	/**
	 * Get content present in a given folder.
	 */
	public List<Content> getContents(String folderId) {
		return daoService.getContents(folderId);
	}

	/**
	 * Get the pieces of content available at that path.
	 */
	public List<Content> getContentsByPath(String path) {
		return daoService.getContentsByPath(path);

	}

	/**
	 * Create a new attachment.
	 */
	public String createAttachment(String creator, GregorianCalendar created,
			ContentStream contentStream) {
		NemakiAttachment na = new NemakiAttachment();
		na.setMimeType(contentStream.getMimeType());		
		//calculate attachment's size
		if (contentStream.getBigLength() == null) {
			na.setLength(contentStream.getLength());
		}else{
			na.setLength(contentStream.getLength());
		}
		na.setCreator(creator);
		na.setCreated(created);
		na.setName(contentStream.getFileName());
		na.setType(NemakiAttachment.TYPE);
		daoService.create(na);
		daoService
		.createAttachment(
				na.getId(),
				na.getRevision(),
				new AttachmentInputStream("content", contentStream
						.getStream(), contentStream.getMimeType(),
						contentStream.getLength()));
		return na.getId();
	}

	/**
	 * Get the folders present in the given folder.
	 */
	public List<Folder> getFolders(String folderId) {
		return daoService.getFolders(folderId);
	}

	/**
	 * Get the documents present in the given folder.
	 */
	public List<Document> getDocuments(String folderId) {
		return daoService.getDocuments(folderId);

	}

	/**
	 * Get an attachment.
	 */
	public NemakiAttachment getAttachment(String attachmentId) {
		return daoService.getAttachment(NemakiAttachment.class, attachmentId);
	}

	/**
	 * Get the stream of an attachment.
	 */
	public InputStream getAttachmentInputStream(String attachmentId,
			String attachmentName) {
		return daoService
				.getAttachmentInputStream(attachmentId, attachmentName);
	}

	/**
	 * Get all content identifiers.
	 */
	public List<String> allIds() {
		return daoService.getAllDocIds();
	}

	public void setDaoService(DaoService daoService) {
		this.daoService = daoService;
	}

}
