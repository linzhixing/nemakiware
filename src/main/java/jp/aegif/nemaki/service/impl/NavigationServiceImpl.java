package jp.aegif.nemaki.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Folder;
import jp.aegif.nemaki.service.NavigationService;
import jp.aegif.nemaki.service.NodeService;
import jp.aegif.nemaki.service.ObjectService;
import jp.aegif.nemaki.service.PermissionService;
import jp.aegif.nemaki.service.RepositoryService;

import org.apache.chemistry.opencmis.commons.data.ExtensionsData;
import org.apache.chemistry.opencmis.commons.data.ObjectData;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderContainer;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderData;
import org.apache.chemistry.opencmis.commons.data.ObjectInFolderList;
import org.apache.chemistry.opencmis.commons.data.ObjectList;
import org.apache.chemistry.opencmis.commons.data.ObjectParentData;
import org.apache.chemistry.opencmis.commons.data.PermissionMapping;
import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships;
import org.apache.chemistry.opencmis.commons.exceptions.CmisInvalidArgumentException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderContainerImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectInFolderListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ObjectParentDataImpl;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.ObjectInfoHandler;

public class NavigationServiceImpl implements NavigationService {

	private NodeService nodeService;
	private ObjectService objectService;
	private PermissionService permissionService;
	private RepositoryService repositoryService;

	public ObjectInFolderList getChildren(CallContext callContext,
			String folderId, String filter, Boolean includeAllowableActions,
			Boolean includePathSegments, BigInteger maxItems,
			BigInteger skipCount, ObjectInfoHandler objectInfos) {
		return getChildrenInternal(callContext, folderId, filter,
				includeAllowableActions, false, includePathSegments, maxItems,
				skipCount, objectInfos);
	}

	public List<ObjectInFolderContainer> getDescendants(
			CallContext callContext, String folderId, BigInteger depth,
			String filter, Boolean includeAllowableActions,
			Boolean includePathSegment, ObjectInfoHandler objectInfos,
			boolean foldersOnly) {

		// check depth
		int d = (depth == null ? 2 : depth.intValue());
		if (d == 0) {
			throw new CmisInvalidArgumentException("Depth must not be 0!");
		}
		if (d < -1) {
			d = -1;
		}

		// set defaults if values not set
		boolean iaa = (includeAllowableActions == null ? false
				: includeAllowableActions.booleanValue());
		boolean ips = (includePathSegment == null ? false : includePathSegment
				.booleanValue());

		// get the tree.
		return getDescendantsInternal(callContext, folderId, filter, iaa,
				false, IncludeRelationships.NONE, null, ips, 0, d, false,
				objectInfos);
	}

	public ObjectData getFolderParent(CallContext callContext, String folderId,
			String filter, ObjectInfoHandler objectInfos) {

		Folder folder = (Folder) nodeService.get(Folder.class, folderId);
		Content parent = nodeService.get(Content.class, folder.getParentId());
		return objectService.compileObjectType(callContext, parent, null, true,
				true, objectInfos);
	}

	public List<ObjectParentData> getObjectParents(CallContext callContext,
			String objectId, String filter, Boolean includeAllowableActions,
			Boolean includeRelativePathSegment, ObjectInfoHandler objectInfos) {

		// Map<String, Object> node = getNode(objectId);
		Content content = nodeService.get(Content.class, objectId);

		// return empty list if content is parent
		if (Folder.TYPE.equals(content.getType())
				&& repositoryService.getRepositoryInfo().getRootFolderId()
						.equals(content.getPath())) {
			return Collections.emptyList();
		}

		// String parentId = (String) content.get("parentId");
		// Map<String, Object> parent = getNode(parentId);
		Content parent = nodeService.get(Content.class, content.getParentId());

		// set object info of the the object
		if (callContext.isObjectInfoRequired()) {
			objectService.compileObjectType(callContext, content, null, false,
					false, objectInfos);
		}

		// set defaults if values not set
		boolean iaa = (includeAllowableActions == null ? false
				: includeAllowableActions.booleanValue());
		boolean irps = (includeRelativePathSegment == null ? false
				: includeRelativePathSegment.booleanValue());
		ObjectData object = objectService.compileObjectType(callContext,
				parent, filter, iaa, false, objectInfos);
		ObjectParentDataImpl result = new ObjectParentDataImpl();
		result.setObject(object);

		if (irps) {
			result.setRelativePathSegment(content.getName());
		}

		return Collections.singletonList((ObjectParentData) result);
	}

	public ObjectList getCheckedOutDocs(CallContext callContext,
			String folderId, String filter, String orderBy,
			Boolean includeAllowableActions,
			IncludeRelationships includeRelationships, String renditionFilter,
			BigInteger maxItems, BigInteger skipCount, ExtensionsData extension) {

		ObjectListImpl result = new ObjectListImpl();
		result.setHasMoreItems(false);
		result.setNumItems(BigInteger.ZERO);
		List<ObjectData> emptyList = Collections.emptyList();
		result.setObjects(emptyList);
		return result;
	}

	private ObjectInFolderList getChildrenInternal(CallContext callContext,
			String folderId, String filter, Boolean includeAllowableActions,
			Boolean includeAcl, Boolean includePathSegments,
			BigInteger maxItems, BigInteger skipCount,
			ObjectInfoHandler objectInfos) {

		// skip and max
		int skip = (skipCount == null ? 0 : skipCount.intValue());
		if (skip < 0) {
			skip = 0;
		}
		int max = (maxItems == null ? Integer.MAX_VALUE : maxItems.intValue());
		if (max < 0) {
			max = Integer.MAX_VALUE;
		}

		Folder folder = (Folder) nodeService.get(Folder.class, folderId);
		List<Content> contents = permissionService.getFiltered(
				callContext.getUsername(),
				PermissionMapping.CAN_VIEW_CONTENT_OBJECT,
				nodeService.getContents(folder.getId()));

		// prepare result
		ObjectInFolderListImpl result = new ObjectInFolderListImpl();
		result.setObjects(new ArrayList<ObjectInFolderData>());
		result.setHasMoreItems(false);

		// iterate through children
		for (Content content : contents) {
			if (skip-- > 0) // Skip as many of the first results as asked by
							// skipCount
				continue;
			if (result.getObjects().size() >= max) {
				result.setHasMoreItems(true);
				continue;
			}
			// build and add child object
			ObjectInFolderDataImpl objectInFolder = new ObjectInFolderDataImpl();
			objectInFolder.setObject(objectService.compileObjectType(
					callContext, content, filter, includeAllowableActions,
					includeAcl, objectInfos));
			if (includePathSegments) {
				String name = content.getName();
				objectInFolder.setPathSegment(name);
			}
			result.getObjects().add(objectInFolder);
		}
		result.setNumItems(BigInteger.valueOf(contents.size()));
		return result;
	}

	private List<ObjectInFolderContainer> getDescendantsInternal(
			CallContext callContext, String folderId, String filter,
			Boolean includeAllowableActions, Boolean includeAcl,
			IncludeRelationships includeRelationships, String renditionFilter,
			Boolean includePathSegments, int level, int maxLevels,
			boolean folderOnly, ObjectInfoHandler objectInfos) {

		List<ObjectInFolderContainer> childrenOfFolderId = null;
		if (maxLevels == -1 || level < maxLevels) {
			ObjectInFolderList children = getChildrenInternal(callContext,
					folderId, filter, includeAllowableActions, includeAcl,
					includePathSegments, BigInteger.valueOf(1000),
					BigInteger.valueOf(0), objectInfos);

			childrenOfFolderId = new ArrayList<ObjectInFolderContainer>();
			if (null != children) {

				for (ObjectInFolderData child : children.getObjects()) {
					ObjectInFolderContainerImpl oifc = new ObjectInFolderContainerImpl();
					String childId = child.getObject().getId();
					List<ObjectInFolderContainer> subChildren = getDescendantsInternal(
							callContext, childId, filter,
							includeAllowableActions, includeAcl,
							includeRelationships, renditionFilter,
							includePathSegments, level + 1, maxLevels,
							folderOnly, objectInfos);

					oifc.setObject(child);
					if (null != subChildren)
						oifc.setChildren(subChildren);
					childrenOfFolderId.add(oifc);
				}
			}
		}
		return childrenOfFolderId;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setObjectService(ObjectService objectService) {
		this.objectService = objectService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

}
