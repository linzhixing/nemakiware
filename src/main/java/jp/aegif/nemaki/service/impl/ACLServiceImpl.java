/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Permission;
import jp.aegif.nemaki.service.ACLService;
import jp.aegif.nemaki.service.NodeService;

import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlEntryImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlListImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AccessControlPrincipalDataImpl;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Discovery Service implementation for CouchDB.
 * 
 */
public class ACLServiceImpl implements ACLService {

	private NodeService nodeService;

	public Acl applyAcl(CallContext callContext, String objectId, Acl aces,
			AclPropagation aclPropagation) throws Exception {
		return updateAcl(callContext, objectId, aces, aclPropagation);
	}

	public Acl getAcl(CallContext callContext, String objectId) {
		Content content = nodeService.get(Content.class, objectId);
		return compileAcl(content);
	}

	public Acl compileAcl(Content content) {

		AccessControlListImpl result = new AccessControlListImpl();
		result.setAces(new ArrayList<Ace>());

		if (content.getPermission() == null)
			return result;

		JSONArray arr = content.getPermission().getEntries();
		for (@SuppressWarnings("unchecked")
		Iterator<Map<String, String>> it = arr.iterator(); it.hasNext();) {
			Map<String, String> map = it.next();
			for (Iterator<String> k = map.keySet().iterator(); k.hasNext();) {
				String key = k.next();
				AddAce(result, key, map.get(key));
			}
		}
		return result;
	}

	/**
	 * Adds an ACE (Access Control Entry) to an ACL (Access Control List).
	 * 
	 * @param accessControlList
	 *            The ACL to modify.
	 */
	private void AddAce(AccessControlListImpl accessControlList,
			String principalId, String permission) {
		AccessControlEntryImpl entry = new AccessControlEntryImpl();
		entry.setPrincipal(new AccessControlPrincipalDataImpl(principalId));
		entry.setPermissions(new ArrayList<String>());
		entry.getPermissions().add(permission);
		entry.setDirect(true);
		accessControlList.getAces().add(entry);
	}

	/**
	 * Updates an ACL (Access Control List)
	 * 
	 * @param id
	 *            Identifier of the content whose ACL needs to be updated.
	 * 
	 *            TODO create AclService and set by DI <br/>
	 *            TODO refactor "ROLE" <br/>
	 *            TODO refactor "GROUP"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private Acl updateAcl(CallContext context, String id, Acl aces,
			AclPropagation aclPropagation) throws Exception {
		Content content = nodeService.get(Content.class, id);
		if (content == null) {
			throw new Exception("updateAcl: id not found");
		} else {
			Permission permission = new Permission();
			// JSONArray userPermissions = new JSONArray();
			JSONArray permissionEntries = new JSONArray();
			for (Ace ace : aces.getAces()) {
				JSONObject json = new JSONObject();
				json.put(ace.getPrincipalId(), ace.getPermissions().get(0));
				permissionEntries.add(json);
			}
			permission.setEntries(permissionEntries);
			content.setPermission(permission);
			nodeService.update(content);
		}
		return getAcl(context, id);
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

}
