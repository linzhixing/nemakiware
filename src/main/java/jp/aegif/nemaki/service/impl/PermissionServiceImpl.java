/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Group;
import jp.aegif.nemaki.model.Permission;
import jp.aegif.nemaki.model.Role;
import jp.aegif.nemaki.repository.NemakiRepositoryInfoImpl;
import jp.aegif.nemaki.service.PermissionService;
import jp.aegif.nemaki.service.UserGroupService;

import org.apache.chemistry.opencmis.commons.data.PermissionMapping;
import org.apache.chemistry.opencmis.commons.definitions.PermissionDefinition;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.PermissionDefinitionDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.PermissionMappingDataImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Permission Service implementation.
 * 
 */
public class PermissionServiceImpl implements PermissionService {

	private static final Log log = LogFactory
			.getLog(PermissionServiceImpl.class);

	private String defaultAdminUserName;
	private NemakiRepositoryInfoImpl repositoryInfo;
	private Map<String, List<String>> roleMap;
	private UserGroupService userGroupService;

	/**
	 * Get default permission for content to be created.
	 */
	public Permission getDefaultPermission(String username) {
		Permission permission = new Permission();
		JSONArray permissionEntries = new JSONArray();
		JSONObject json;

		json = new JSONObject();
		json.put(UserGroupService.GROUP_EVERYONE, Role.CONSUMER);
		permissionEntries.add(json);

		json = new JSONObject();
		json.put(username, Role.OWNER);
		permissionEntries.add(json);

		permission.setEntries(permissionEntries);

		return permission;
	}

	/**
	 * Get the list of existing permissions. For instance: read, write, all
	 */
	public List<PermissionDefinition> getPermissionDefinitions() {
		List<PermissionDefinition> permissions = new ArrayList<PermissionDefinition>();
		permissions.add(createPermission(CMIS_READ_PERMISSION, "Read"));
		permissions.add(createPermission(CMIS_WRITE_PERMISSION, "Write"));
		permissions.add(createPermission(CMIS_ALL_PERMISSION, "All"));
		return permissions;
	}

	/**
	 * Create a new permission.
	 */
	private PermissionDefinition createPermission(String permission,
			String description) {
		PermissionDefinitionDataImpl pd = new PermissionDefinitionDataImpl();
		pd.setPermission(permission);
		pd.setDescription(description);
		return pd;
	}

	/**
	 * Mapping permission group to elemental permission (repository static)
	 */
	public Map<String, PermissionMapping> getPermissionMap() {
		List<PermissionMapping> list = new ArrayList<PermissionMapping>();
		list.add(createPermissionMapping(
				PermissionMapping.CAN_CREATE_DOCUMENT_FOLDER,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_CREATE_FOLDER_FOLDER,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_DELETE_CONTENT_DOCUMENT,
				CMIS_WRITE_PERMISSION));
		list.add(createPermissionMapping(PermissionMapping.CAN_DELETE_OBJECT,
				CMIS_ALL_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_DELETE_TREE_FOLDER, CMIS_ALL_PERMISSION));
		list.add(createPermissionMapping(PermissionMapping.CAN_GET_ACL_OBJECT,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_ALL_VERSIONS_VERSION_SERIES,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_CHILDREN_FOLDER, CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_DESCENDENTS_FOLDER,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_FOLDER_PARENT_OBJECT,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_PARENTS_FOLDER, CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_GET_PROPERTIES_OBJECT,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(PermissionMapping.CAN_MOVE_OBJECT,
				CMIS_WRITE_PERMISSION));
		list.add(createPermissionMapping(PermissionMapping.CAN_MOVE_SOURCE,
				CMIS_READ_PERMISSION));
		list.add(createPermissionMapping(PermissionMapping.CAN_MOVE_TARGET,
				CMIS_WRITE_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_SET_CONTENT_DOCUMENT,
				CMIS_WRITE_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_UPDATE_PROPERTIES_OBJECT,
				CMIS_WRITE_PERMISSION));
		list.add(createPermissionMapping(
				PermissionMapping.CAN_VIEW_CONTENT_OBJECT, CMIS_READ_PERMISSION));
		Map<String, PermissionMapping> map = new LinkedHashMap<String, PermissionMapping>();
		for (PermissionMapping pm : list) {
			map.put(pm.getKey(), pm);
		}
		return map;
	}

	/**
	 * Create a new permission mapping.
	 */
	private PermissionMapping createPermissionMapping(String key,
			String permission) {
		PermissionMappingDataImpl pm = new PermissionMappingDataImpl();
		pm.setKey(key);
		pm.setPermissions(Collections.singletonList(permission));
		return pm;
	}

	/**
	 * Filter contents using permission entries
	 * 
	 * @param username
	 *            user who is doing operation <br/>
	 *            (e.g. admin)
	 * @param operation
	 *            also represents permission mapping key <br/>
	 *            (e.g. CAN_CREATE_DOCUMENT_FOLDER)
	 * @param contents
	 *            original contents which is got from DB
	 */
	public List<Content> getFiltered(String username, String operation,
			List<Content> contents) {

		Map<String, Content> map = new HashMap<String, Content>();

		// Names of the groups this user directly belongs to.
		Set<String> groupIds = userGroupService
				.getGroupsContainingUser(username);
		// Add parent groups to the list of groups to check
		Set<Group> groupsIncludingParents = userGroupService
				.getGroupsIncludingParents(groupIds);

		for (Content content : contents) {
			for (Object permissionEntryObject : content.getPermission()
					.getEntries()) {
				Map permissionEntry = (Map) permissionEntryObject;
				// get permission (e.g. cmis:read) which is allowed for the
				// operation (e.g. canViewContent.Object)
				String permission = repositoryInfo.getAclCapabilities()
						.getPermissionMapping().get(operation).getPermissions()
						.get(0);

				// add all contents in map if user is admin
				if (defaultAdminUserName.equals(username)) {
					content.setRole(Role.OWNER); // mryoshio: should modify not
													// to change content here
					map.put(content.getId(), content);
					break;
				}

				// add into filtered contents if the user can
				if (isExecutable(username, permissionEntry, permission)) {
					content.setRole(Role.valueOf((String) permissionEntry
							.get(username))); // NICOLAS ASKS: WHY MODIFY
												// CONTENT? Check if role is
												// added to database. If it is,
												// fix by cloning object
					map.put(content.getId(), content);
					break;
				}
				/*
				 * add content into filtered contents if the groups the user
				 * belongs to can entry loop continues while isExecutable(group,
				 * ...) is true because user permission overrides group
				 * permission
				 */
				for (Group group : groupsIncludingParents) {
					if (isExecutable(group.getId(), permissionEntry, permission)) {
						String permissionEntryforThisGroup = (String) permissionEntry
								.get(group.getId());
						Role role = Role.valueOf(permissionEntryforThisGroup);
						content.setRole(role); // mryoshio: should modify not to
												// change content here
						map.put(content.getId(), content);
						log.debug("Granted: group " + group.getName()
								+ ", permission " + permission + ", entry "
								+ permissionEntry);
						break;
					} else {
						log.debug("Not granted: group " + group.getName()
								+ ", permission " + permission + ", entry "
								+ permissionEntry);
					}
				}
			}
		}
		List<Content> filtered = new ArrayList<Content>();
		for (Map.Entry<String, Content> entry : map.entrySet()) {
			filtered.add(entry.getValue());
		}
		return filtered;
	}

	/**
	 * Checks whether userOrGroup has a given permission on a given entry FIXME:
	 * id is sometimes compared with name.
	 */
	private boolean isExecutable(String userOrGroup, Map entry,
			String permission) {
		String role;
		log.debug("#isExecutable userOrGroup: " + userOrGroup + " entry: "
				+ entry + " permission:" + permission);

		if (entry.containsKey(userOrGroup)) { // FIXME: entry contains things
												// like
												// {GROUP_EVERYONE=CONSUMER}
												// whereas userOrGroup is like
												// 3b4c227000ee42a91535243bf4001d21
			role = (String) entry.get(userOrGroup);
		} else {
			log.debug("false");
			return false;
		}
		// search for role (e.g. COLLABORATOR)
		// which has the permission (e.g. cmis:write)
		if (roleMap.containsKey(permission)
				&& roleMap.get(permission).contains(role)) {
			log.debug("true");
			return true;
		}

		log.debug("false");
		return false;
	}

	/**
	 * TODO change this to use DI configuration
	 */
	public Set<Action> getAllowableActionset(Role role, boolean isFolder,
			boolean isRoot) {

		Set<Action> actionSet = new HashSet<Action>();

		switch (role) {
		case CONSUMER:
			setConsumerActions(actionSet, isFolder, isRoot);
			break;
		case COLLABORATOR:
			setConsumerActions(actionSet, isFolder, isRoot);
			setCollaboratorActions(actionSet, isFolder);
			break;
		case OWNER:
			setConsumerActions(actionSet, isFolder, isRoot);
			setCollaboratorActions(actionSet, isFolder);
			setOwnerActions(actionSet);
			break;
		default:
		}
		return actionSet;
	}

	public void setDefaultAdminUserName(String defaultAdminUserName) {
		this.defaultAdminUserName = defaultAdminUserName;
	}

	public void setRepositoryInfo(NemakiRepositoryInfoImpl repositoryInfo) {
		this.repositoryInfo = repositoryInfo;
	}

	public void setRoleMap(Map<String, List<String>> roleMap) {
		this.roleMap = roleMap;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	private void setConsumerActions(Set<Action> actionSet, boolean isFolder,
			boolean isRoot) {
		addAction(actionSet, Action.CAN_GET_OBJECT_PARENTS, !isRoot);
		addAction(actionSet, Action.CAN_GET_PROPERTIES, true);
		addAction(actionSet, Action.CAN_GET_ACL, true);
		if (isFolder) {
			addAction(actionSet, Action.CAN_GET_DESCENDANTS, true);
			addAction(actionSet, Action.CAN_GET_CHILDREN, true);
			addAction(actionSet, Action.CAN_GET_FOLDER_PARENT, !isRoot);
			addAction(actionSet, Action.CAN_GET_FOLDER_TREE, true);
		} else {
			addAction(actionSet, Action.CAN_GET_CONTENT_STREAM, true);
			addAction(actionSet, Action.CAN_GET_ALL_VERSIONS, true);
		}
	}

	private void setCollaboratorActions(Set<Action> actionSet, boolean isFolder) {
		addAction(actionSet, Action.CAN_UPDATE_PROPERTIES, true);
		addAction(actionSet, Action.CAN_DELETE_OBJECT, true);
		addAction(actionSet, Action.CAN_MOVE_OBJECT, true);
		if (isFolder) {
			addAction(actionSet, Action.CAN_CREATE_DOCUMENT, true);
			addAction(actionSet, Action.CAN_CREATE_FOLDER, true);
			addAction(actionSet, Action.CAN_DELETE_TREE, true);
		} else {
			addAction(actionSet, Action.CAN_DELETE_CONTENT_STREAM, true);
			addAction(actionSet, Action.CAN_SET_CONTENT_STREAM, true);
		}
	}

	private void setOwnerActions(Set<Action> actionSet) {
		addAction(actionSet, Action.CAN_APPLY_ACL, true);
		addAction(actionSet, Action.CAN_APPLY_POLICY, true);
	}

	private void addAction(Set<Action> actionSet, Action action,
			boolean condition) {
		if (condition)
			actionSet.add(action);
	}

}
