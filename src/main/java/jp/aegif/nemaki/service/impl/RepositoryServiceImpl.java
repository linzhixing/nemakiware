package jp.aegif.nemaki.service.impl;

import java.math.BigInteger;
import java.util.List;

import jp.aegif.nemaki.repository.NemakiRepositoryInfoImpl;
import jp.aegif.nemaki.repository.TypeManager;
import jp.aegif.nemaki.service.PermissionService;
import jp.aegif.nemaki.service.RepositoryService;

import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionContainer;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionList;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.CapabilityAcl;
import org.apache.chemistry.opencmis.commons.enums.CapabilityChanges;
import org.apache.chemistry.opencmis.commons.enums.CapabilityContentStreamUpdates;
import org.apache.chemistry.opencmis.commons.enums.CapabilityJoin;
import org.apache.chemistry.opencmis.commons.enums.CapabilityQuery;
import org.apache.chemistry.opencmis.commons.enums.CapabilityRenditions;
import org.apache.chemistry.opencmis.commons.enums.SupportedPermissions;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.AclCapabilitiesDataImpl;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.RepositoryCapabilitiesImpl;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.springframework.beans.factory.InitializingBean;

public class RepositoryServiceImpl implements RepositoryService,
		InitializingBean {

	private String namespace;
	private PermissionService permissionService;
	private NemakiRepositoryInfoImpl repositoryInfo;
	private TypeManager typeManager;

	public TypeManager getTypeManager() {
		return typeManager;
	}

	public boolean hasThisRepositoryId(String repositoryId) {
		return (repositoryId.equals(repositoryInfo.getId()));
	}

	public NemakiRepositoryInfoImpl getRepositoryInfo() {
		return repositoryInfo;
	}

	public String getNamespace() {
		return namespace;
	}

	public TypeDefinitionList getTypeChildren(CallContext callContext,
			String typeId, Boolean includePropertyDefinitions,
			BigInteger maxItems, BigInteger skipCount) {

		return typeManager.getTypesChildren(callContext, typeId,
				includePropertyDefinitions, maxItems, skipCount);
	}

	public List<TypeDefinitionContainer> getTypeDescendants(
			CallContext callContext, String typeId, BigInteger depth,
			Boolean includePropertyDefinitions) {
		return typeManager.getTypesDescendants(callContext, typeId, depth,
				includePropertyDefinitions);
	}

	public TypeDefinition getTypeDefinition(CallContext callContext,
			String typeId) {
		return typeManager.getTypeDefinition(callContext, typeId);
	}

	/**
	 * Sets CMIS optional capabilities for Nemaki repository.
	 */
	public void afterPropertiesSet() throws Exception {

		RepositoryCapabilitiesImpl capabilities = new RepositoryCapabilitiesImpl();

		// Navigation Capabilities
		capabilities.setSupportsGetDescendants(true);
		capabilities.setSupportsGetFolderTree(true);

		// Object Capabilities
		capabilities
				.setCapabilityContentStreamUpdates(CapabilityContentStreamUpdates.ANYTIME);
		capabilities.setCapabilityChanges(CapabilityChanges.NONE);
		// TODO setCapabilityRendition will be enalbed in the future release.
		capabilities.setCapabilityRendition(CapabilityRenditions.NONE);

		// Filling Capabilities
		capabilities.setSupportsMultifiling(false);
		capabilities.setSupportsUnfiling(false);
		capabilities.setSupportsVersionSpecificFiling(false);

		// Versioning Capabilities
		capabilities.setIsPwcUpdatable(false);
		capabilities.setIsPwcSearchable(false);
		capabilities.setAllVersionsSearchable(false);

		// Query Capabilities
		capabilities.setCapabilityQuery(CapabilityQuery.BOTHCOMBINED);
		capabilities.setCapabilityJoin(CapabilityJoin.NONE);

		// ACL Capabilities
		capabilities.setCapabilityAcl(CapabilityAcl.MANAGE);

		repositoryInfo.setCapabilities(capabilities);

		// ACL Capabilities Details
		AclCapabilitiesDataImpl aclCapability = new AclCapabilitiesDataImpl();
		aclCapability.setSupportedPermissions(SupportedPermissions.BASIC);
		aclCapability.setAclPropagation(AclPropagation.PROPAGATE);
		aclCapability.setPermissionDefinitionData(permissionService
				.getPermissionDefinitions());
		aclCapability.setPermissionMappingData(permissionService
				.getPermissionMap());
		repositoryInfo.setAclCapabilities(aclCapability);
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	public void setRepositoryInfo(NemakiRepositoryInfoImpl repositoryInfo) {
		this.repositoryInfo = repositoryInfo;
	}

	public void setTypeManager(TypeManager typeManager) {
		this.typeManager = typeManager;
	}
}
