/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service.impl;

import java.io.InputStream;
import java.util.List;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Document;
import jp.aegif.nemaki.model.Folder;
import jp.aegif.nemaki.model.Group;
import jp.aegif.nemaki.model.NemakiAttachment;
import jp.aegif.nemaki.model.User;
import jp.aegif.nemaki.service.DaoService;
import jp.aegif.nemaki.service.db.CouchConnector;

import org.ektorp.AttachmentInputStream;
import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.springframework.stereotype.Component;

/**
 * Dao Service implementation for CouchDB.
 * 
 * @author mryoshio
 * 
 */
@Component
public class CouchDaoServiceImpl implements DaoService {

	private CouchDbConnector connector;

	public Content get(Class<?> clazz, String objectId) {
		return (Content) connector.get(clazz, objectId);
	}

	public void create(Content content) {
		connector.create(content);
	}

	public void update(Content content) {
		connector.update(content);
	}

	public void delete(Class<Content> clazz, String objectId) {
		Content content = connector.get(clazz, objectId);
		connector.delete(content);
		
	}
	
	public void deleteUser(Class<User> clazz, String objectId) {
		User user = connector.get(clazz, objectId);
		connector.delete(user);
	}
	
	public void deleteGroup(Class<Group> clazz, String objectId) {
		Group group = connector.get(clazz, objectId);
		connector.delete(group);
	}

	public List<User> getUsers() {
		ViewQuery query = new ViewQuery().designDocId("_design/_repo")
				.viewName("usersByName");
		return connector.queryView(query, User.class);
	}

	public List<Content> getContents(String parentId) {
		ViewQuery query = new ViewQuery().designDocId("_design/_repo")
				.viewName("children").key(parentId);
		return connector.queryView(query, Content.class);
	}

	public List<Content> getContentsByPath(String path) {
		ViewQuery vq = new ViewQuery().designDocId("_design/_repo")
				.viewName("foldersByPath").key(path);
		return connector.queryView(vq, Content.class);
	}

	public void createAttachment(String id, String revision,
			AttachmentInputStream attachmentInputStream) {
		connector.createAttachment(id, revision, attachmentInputStream);
	}

	public List<Folder> getFolders(String parentFolderId) {
		ViewQuery query;
		if (parentFolderId == null) {
			query = new ViewQuery().designDocId("_design/_repo").viewName(
					"folders");
		} else {
			query = new ViewQuery().designDocId("_design/_repo")
					.viewName("folders").key(parentFolderId);
		}
		return connector.queryView(query, Folder.class);
	}

	public List<Document> getDocuments(String folderId) {
		ViewQuery query;
		if (folderId == null) {
			query = new ViewQuery().designDocId("_design/_repo").viewName(
					"documents");
		} else {
			query = new ViewQuery().designDocId("_design/_repo")
					.viewName("documents").key(folderId);
		}
		return connector.queryView(query, Document.class);
	}

	public NemakiAttachment getAttachment(Class<NemakiAttachment> clazz,
			String attachmentId) {
		return connector.get(clazz, attachmentId);
	}

	public InputStream getAttachmentInputStream(String attachmentId,
			String attachmentName) {
		return connector.getAttachment(attachmentId, attachmentName);
	}

	public User getUserByName(String username) {
		ViewQuery query = new ViewQuery().designDocId("_design/_repo")
				.viewName("usersByName").key(username);
		List<User> users = connector.queryView(query, User.class);
		return users.get(0);
	}

	public User getUserById(String userId) {
		return connector.get(User.class, userId);
	}
	
	public Group getGroupById(String groupId) {
		return connector.get(Group.class, groupId);
	}

	public List<Group> getGroups() {
		ViewQuery query = new ViewQuery().designDocId("_design/_repo")
				.viewName("groupById");
		return connector.queryView(query, Group.class);
	}

	public List<String> getAllDocIds() {
		return connector.getAllDocIds();
	}
	
	public void setConnector(CouchConnector connector) {
		this.connector = connector.getConnection();
	}
}
