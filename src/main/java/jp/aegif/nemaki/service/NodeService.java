/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service;

import java.io.InputStream;
import java.util.GregorianCalendar;
import java.util.List;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Document;
import jp.aegif.nemaki.model.Folder;
import jp.aegif.nemaki.model.NemakiAttachment;

import org.apache.chemistry.opencmis.commons.data.ContentStream;

/**
 * Node Service interface.
 */
public interface NodeService {

	/**
	 * Get one piece of CMIS content.
	 * 
	 * @clazz Type of CMIS content expected, must inherit from Content.
	 */
	Content get(Class<?> clazz, String objectId);

	/**
	 * Get content present in a given folder.
	 */
	List<Content> getContents(String folderId);

	/**
	 * Create one content
	 */
	void create(Content content);

	/**
	 * Update one content
	 */
	void update(Content content);

	/**
	 * Delete Content
	 */
	void delete(Class<Content> clazz, String objectId);

	/**
	 * Get an attachment.
	 */
	NemakiAttachment getAttachment(String attachmentId);

	/**
	 * Create a new attachment.
	 */
	String createAttachment(String user, GregorianCalendar created,
			ContentStream contentStream);

	/**
	 * Get the pieces of content available at that path.
	 */
	List<Content> getContentsByPath(String path);

	/**
	 * Get the folders present in the given folder.
	 */
	List<Folder> getFolders(String parentId);

	/**
	 * Get the documents present in the given folder.
	 */
	List<Document> getDocuments(String folderId);

	/**
	 * Get the stream of an attachment.
	 */
	InputStream getAttachmentInputStream(String attachmentId,
			String attachmentName);

	/**
	 * Get all content identifiers.
	 */
	List<String> allIds();

}
