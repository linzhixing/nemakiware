/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
package jp.aegif.nemaki.service;

import java.io.InputStream;
import java.util.List;

import jp.aegif.nemaki.model.Content;
import jp.aegif.nemaki.model.Document;
import jp.aegif.nemaki.model.Folder;
import jp.aegif.nemaki.model.Group;
import jp.aegif.nemaki.model.NemakiAttachment;
import jp.aegif.nemaki.model.User;

import org.ektorp.AttachmentInputStream;

/**
 * DAO Service interface.
 */
public interface DaoService {

	/**
	 * Get a content
	 */
	Content get(Class<?> clazz, String objectId);

	/**
	 * Create a content
	 */
	void create(Content content);

	/**
	 * Update a content
	 */
	void update(Content content);

	/**
	 * Delete a content
	 */
	void delete(Class<Content> clazz, String objectId);
	
	/**
	 * Delete a user 
	 */
	void deleteUser(Class<User> clazz, String userId);

	/**
	 * Delete a group
	 */
	void deleteGroup(Class<Group> clazz, String groupId);
	
	/**
	 * Get all users from DB
	 */
	List<User> getUsers();

	/**
	 * Get contents in the parent with parentId
	 */
	List<Content> getContents(String parentId);

	/**
	 * Get contents by path
	 */
	List<Content> getContentsByPath(String path);

	/**
	 * Create attachment
	 */
	void createAttachment(String id, String revision,
			AttachmentInputStream attachmentInputStream);

	/**
	 * Get folders in the folder with folderId
	 */
	List<Folder> getFolders(String folderId);

	/**
	 * Get documents in the folder with folderId
	 */
	List<Document> getDocuments(String folderId);

	/**
	 * Get a NemakiAttachment
	 */
	NemakiAttachment getAttachment(Class<NemakiAttachment> clazz,
			String attachmentId);

	/**
	 * Get attachmentInputStream for download
	 */
	InputStream getAttachmentInputStream(String attachmentId,
			String attachmentName);

	/**
	 * Get an user with username
	 */
	User getUserByName(String username);

	/**
	 * Get all groups
	 */
	List<Group> getGroups();

	/**
	 * Get a group, given its id
	 */
	Group getGroupById(String groupId);

	/**
	 * Get all document ids
	 */
	List<String> getAllDocIds();

}
