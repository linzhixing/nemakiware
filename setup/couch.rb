#!/usr/bin/ruby

#
# This file is part of NemakiWare.
#
# NemakiWare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# NemakiWare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
#

require 'net/http'

module Couch
  class Server
    
    attr_accessor :host, :port, :db
    
    def initialize(host, port, db, options = nil)
      @host = host
      @port = port
      @db = db
      @options = options
    end
    
    def delete(uri)
      request(Net::HTTP::Delete.new(uri))
    end
    
    def get(uri)
      request(Net::HTTP::Get.new(uri))
    end
    
    def put(uri, json)
      req = Net::HTTP::Put.new(uri)
      req["content-type"] = "application/json"
      req.body = json
      request(req)
    end
    
    def post(uri, json)
      req = Net::HTTP::Post.new(uri)
      req["content-type"] = "application/json"
      req.body = json
      request(req)
    end
    
    def request(req)
      res = Net::HTTP.start(@host, @port) { |http| http.request(req) }
      unless res.kind_of?(Net::HTTPSuccess) || res.kind_of?(Net::HTTPNotFound)
        handle_error(req, res)
      end
      res
    end
    
    private
    
    def handle_error(req, res)
      e = RuntimeError.new("#{res.code}: #{res.message}\nMETHOD: #{req.method}}\nURI: #{req.path}\n#{res.body}")
      raise e
    end
  end
end
