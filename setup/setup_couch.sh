#!/bin/sh

#
# This file is part of NemakiWare.
#
# NemakiWare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# NemakiWare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
#

set -x

REPOSITORY_ID=books
COUCHDB_HOST=127.0.0.1
MAX_CONTENTS=10

# generate user, document, folder
./gen_data.rb ${REPOSITORY_ID} ${COUCHDB_HOST} ${MAX_CONTENTS} 

cd change_root

# Change root id from "root" to "/"
javac -cp ./lib/jackson-core-asl-1.8.4.jar:./lib/jackson-mapper-asl-1.8.4.jar:./lib/json-simple-1.1.jar:./lib/org.ektorp-1.2.1.jar:.  Permission.java Role.java Content.java ChangeRoot.java
java -cp ./lib/commons-io-2.0.1.jar:./lib/json-simple-1.1.jar:./lib/jackson-core-asl-1.8.4.jar:./lib/jackson-mapper-asl-1.8.4.jar:./lib/org.ektorp-1.2.1.jar:./lib/httpclient-4.1.1.jar:./lib/httpclient-cache-4.1.1.jar:./lib/httpcore-4.1.jar:./lib/commons-logging-1.1.1.jar:./lib/slf4j-api-1.6.2.jar:./lib/slf4j-log4j12-1.6.2.jar:./lib/log4j-1.2.16.jar:. ChangeRoot ${REPOSITORY_ID} ${COUCHDB_HOST}

