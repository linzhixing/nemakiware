#!/usr/bin/env ruby

#
# This file is part of NemakiWare.
#
# NemakiWare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# NemakiWare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
#

# [NOTICE] set '/' on root folder id because we cannot set it in this script

$LOAD_PATH << '.'

require 'rubygems'
require 'digest/md5'
require 'json'
require 'couch'
require 'bcrypt'
require 'set'
require 'base64'
require 'mime/types'
require 'date'

REPOSITORY_ID=ARGV[0]
COUCHDB_HOST=ARGV[1]
FOLDERS_MAX=ARGV[2].to_i
DOCUMENTS_MAX=ARGV[2].to_i
ATTACHMENTS_MAX=3

class LoadData
  
  include Couch
  attr_accessor :server
  
  def initialize(host, port, db)
    @server = Couch::Server.new(host, port, db)
    @root ="/"
    @usernames = ["admin", "taro", "jiro", "saburo", "shiro", "goro", "rokuro", "shichiro"]
    @groupnames = ["A", "B", "C", "D", "E", "F", "EVERYONE"]
    @roles = ["OWNER", "COLLABORATOR", "CONSUMER"]
    @content_props = {
      "name"=>"", "path"=>"dummy",
      "parentId"=> @root, "creator"=> @usernames,
      "modifier"=> @usernames, "created"=>"date", "modified"=>"date"
    }
    @attachment_props = {
      "name"=>"content", "role"=>"CONSUMER",
      "creator"=>"admin", "_attachments"=>""
    }
    @document_props = {
      "parentId"=> "", "permission" => "",
      "nemakiAttachments" => []
    }
    @folder_props = {
      "parentId"=> "", "permission" => ""
    }
    @user_props = {
      "creator"=>"admin", "modifier"=>"admin",
      "email"=> "", "firstName"=> "",
      "lastName"=> "", "passwordHash"=> ""
    }
    @group_props = {
      "creator"=>"admin", "modifier"=>"admin",
      "users"=>"", "groups"=>""
    }
    @view_path = "./views.json"
    @parentIds = [@root]
    @imgs = ["./images/0.jpg", "./images/1.jpg", "./images/2.jpg", "./images/3.jpg", "./images/4.jpg"]
  end
  
  def create_database()
    @server.put("/#{@server.db}/", "")
  end
  
  def delete_database()
    @server.delete("/#{@server.db}")
  end
  
  def create_content(id, doc)
    @server.put("/#{@server.db}/#{id}", doc)
  end
  
  def generate_content(type, idx)
    content = {}
    content["type"] = type
    @content_props.each do |key, val|
      if key == 'name'
        content[key] = "#{type}_#{idx.to_s}_name"
      elsif val == 'date'
        content[key] = Date::today
      elsif key == 'parentId'
        content[key] = @root
      elsif val.instance_of? Array
        content[key] = val[rand(val.size)]
      else
        content[key] = val
      end
    end
    content
  end
  
  def create_attachments(docs)
    attachments = generate_attachments(docs)
    attachments.each do |f|
      create_content(f["id"], f["attachment"].to_json)
    end
  end
  
  def generate_attachments(docs)
    attachments = []
    docs.each_with_index do |doc, idx|
      attachment = doc["document"]["nemakiAttachments"]
      attachment.each do |attach_id|
        a = generate_content("attachment", idx)
        img_name = @imgs[rand(@imgs.size)]
        @attachment_props.each do |key, val|
          if key == '_attachments'
            File.open(img_name) {|f|
              file = f.read
              data = Base64.encode64(file).gsub(/\n/, '')
              mimetype = MIME::Types.type_for(img_name).first.content_type
              a[key] = {"content"=>{ "data"=>data, "content_type"=>mimetype }}
              a['length'] = file.length
              a['mimeType'] = mimetype
            }
          else
            a[key] = val
          end
        end
        attachments.push({ "id" => attach_id, "attachment" => a})
      end
    end
    attachments
  end
  
  def create_root
    f = {}
    f["created"] = Date::today
    f["modified"] = Date::today
    f["creator"] = "admin"
    f["modifier"] = "admin"
    f["name"] = "/"
    f["path"] = "/"
    f["permission"] = {"entries"=>[{ "GROUP_EVERYONE" => "CONSUMER" }]}
    f["type"] = "folder"
    create_content("root", f.to_json)
  end
  
  def create_folders()
    folders = generate_folders()
    folders.each do |f|
      r = create_content(f["id"], f["folder"].to_json)
    end
  end
  
  def generate_folders()
    folders = []
    FOLDERS_MAX.times do |idx|
      f = generate_content("folder", idx)
      @folder_props.each do |key, val|
        if key == 'parentId'
          f[key] = @parentIds[rand(@parentIds.size)]
          if idx % 10 == 0
            @parentIds.push("folder_#{idx}")
          end
        elsif key == 'permission'
          entries = []
          max = rand(3)+1
          max.times do
            entry = {}
            entry[@usernames[rand(@usernames.size)]] = @roles[rand(@roles.size)]
            entries.push(entry)
          end
          entries.push({ "GROUP_EVERYONE" => "CONSUMER" })
          f[key] = { "entries" => entries}
        else
          f[key] = val
        end
      end
      folders.push({ "id" => "folder_#{idx}", "folder" => f})
    end
    folders
  end
  
  def create_documents()
    documents = generate_documents()
    create_attachments(documents)
    documents.each do |d|
      create_content(d["id"], d["document"].to_json)
    end
  end
  
  def generate_documents()
    documents = []
    DOCUMENTS_MAX.times do |idx|
      d = generate_content("document", idx)
      @document_props.each do |key, val|
        if key == 'parentId'
          d[key] = @parentIds[rand(@parentIds.size)]
        elsif key == 'permission'
          entries = []
          max = rand(3)+1
          max.times do
            entry = {}
            entry[@usernames[rand(@usernames.size)]] = @roles[rand(@roles.size)]
            entries.push(entry)
          end
          entries.push({ "GROUP_EVERYONE" => "CONSUMER" })
          d[key] = { "entries" => entries}
        elsif key == 'nemakiAttachments'
          as = []
          i = rand(ATTACHMENTS_MAX)+1
          i.times do |cnt|
            as.push("doc_#{idx.to_s}_attach_#{cnt}")
          end
          d[key] = as
        else
          d[key] = val
        end
      end
      documents.push({ "id"=>"document_#{idx}", "document"=>d})
    end
    documents
  end
  
  def create_groups()
    groups = generate_groups()
    groups.each do |g|
      create_content(g["id"], g["group"].to_json)
    end
  end
  
  def generate_groups()
    groups = []
    @groupnames.each_with_index do |groupname, idx|
      g = generate_content("group", idx)
      @group_props.each do |key, val|
        if key == 'users'
          g[key] = [@usernames[rand(@usernames.size)]]
        elsif key == 'groups'
          g[key] = []
        else
        end
      end
      groups.push({ "id" => "GROUP_#{groupname}", "group" => g})
    end
    groups
  end
  
  def create_users()
    users = generate_users()
    users.each do |u|
      create_content(u["id"], u["user"].to_json)
    end
  end
  
  def generate_users()
    users = []
    @usernames.each_with_index do |username, idx|
      u = generate_content("aegif:user", idx)
      @user_props.each do |key, val|
        if key == 'email'
          u[key] = "#{username}@aegif.jp"
        elsif key == "firstName" or key == "lastName"
          u[key] = username
        elsif key == 'passwordHash'
          u[key] = BCrypt::Password.create(username)
        else
          u[key] = val
        end
      end
      u["name"] = username
      users.push({ "id" => "user_#{idx}", "user" => u})
    end
    users
  end
  
  def create_views()
    open(@view_path) do |f|
      views = JSON.load(f)
      create_content("_design/_repo", views.to_json)
    end
  end
end

ld  = LoadData.new(COUCHDB_HOST, 5984, REPOSITORY_ID)
res = ld.server.get("http://"+ld.server.host+":"+ld.server.port.to_s+"/"+ld.server.db+"/")
if res.kind_of?(Net::HTTPSuccess)
  puts "--- delete existsed DB"
  ld.delete_database()
end
ld.create_database()
ld.create_root()
ld.create_folders()
ld.create_documents()
ld.create_users()
ld.create_groups()

ld.create_views()
