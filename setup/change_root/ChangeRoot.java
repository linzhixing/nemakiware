/**
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NemakiWare. If not, see <http://www.gnu.org/licenses/>.
 */
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ChangeRoot {
	public static void main(String[] args) {

		String repositoryId = args[0];
		String couchHost = args[1];
		int maxConnections = 10;

		// get root folder
		HttpClient httpClient = new StdHttpClient.Builder().host(couchHost)
				.maxConnections(maxConnections).build();
		CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
		CouchDbConnector connector = dbInstance.createConnector(repositoryId,
				false);
		Content rootFolder = connector.get(Content.class, "root");

		// delete old root
		connector.delete(rootFolder);

		// create new root
		Content newRoot = new Content();
		newRoot.setId("/");
		newRoot.setName("/");
		newRoot.setType("folder");
		newRoot.setCreator("admin");
		newRoot.setModifier("admin");
		newRoot.setPath("/");
		newRoot.setLength(0);
		newRoot.setCreated(millisToCalendar(System.currentTimeMillis()));
		newRoot.setModified(millisToCalendar(System.currentTimeMillis()));

		Permission p = new Permission();
		JSONArray ja = new JSONArray();
		List l = new ArrayList();
		Map m = new HashMap();
		m.put("GROUP_EVERYONE", "CONSUMER");
		ja.add(m);
		p.setEntries(ja);
		newRoot.setPermission(p);
		connector.create(newRoot);
	}

	private static GregorianCalendar millisToCalendar(long millis) {
		GregorianCalendar result = new GregorianCalendar();
		result.setTimeZone(TimeZone.getTimeZone("GMT"));
		result.setTimeInMillis(millis);
		return result;
	}
	
}
